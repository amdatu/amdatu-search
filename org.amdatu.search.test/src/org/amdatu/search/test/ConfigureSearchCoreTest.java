/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.test;

import java.io.IOException;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.amdatu.search.Document;
import org.amdatu.search.Index;
import org.amdatu.search.Search;
import org.amdatu.search.SearchResponse;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

public class ConfigureSearchCoreTest extends TestCase {
    private static final String FACTORY_PID = "org.amdatu.search.solr";

    private final BundleContext m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    private final Semaphore m_semaphore = new Semaphore(0);
    private final ConcurrentMap<String, Index> m_indexes = new ConcurrentHashMap<String, Index>();
    private final ConcurrentMap<String, Search> m_searches = new ConcurrentHashMap<String, Search>();

    private volatile Component m_component;
    private volatile DependencyManager m_dm;
    private volatile ConfigurationAdmin m_configAdmin;

    public void testConfigureSingleCoreAndCheckLifeCycleAndSimpleUsage() throws Exception {
        String repositoryName = "test";

        Configuration config = configureCore(repositoryName, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
        		"<schema name=\"default\" version=\"1.2\">" + 
        		"    <types>" + 
        		"        <fieldType name=\"string\" class=\"solr.StrField\" sortMissingLast=\"true\" omitNorms=\"true\" />" + 
        		"        <fieldType name=\"boolean\" class=\"solr.BoolField\" sortMissingLast=\"true\" omitNorms=\"true\" />" + 
        		"        <fieldType name=\"int\" class=\"solr.TrieIntField\" precisionStep=\"0\" omitNorms=\"true\" positionIncrementGap=\"0\" />" + 
        		"        <fieldType name=\"uri\" class=\"solr.TextField\" positionIncrementGap=\"100\"/>" + 
        		"    </types>" + 
        		"    <fields>" + 
        		"        <field name=\"uri\" type=\"uri\" stored=\"true\" indexed=\"true\" required=\"true\" />" + 
        		"        <field name=\"title\" type=\"string\" stored=\"true\" indexed=\"true\" />" + 
        		"        <field name=\"description\" type=\"string\" stored=\"true\" indexed=\"true\" />" + 
                "        <field name=\"identifier\" type=\"string\" stored=\"true\" indexed=\"true\" />" + 
                "        <field name=\"content\" type=\"string\" stored=\"true\" indexed=\"true\" multiValued=\"true\" />" + 
        		"    </fields>" + 
        		"    <uniqueKey>identifier</uniqueKey>" + 
        		"    <copyField source=\"uri\" dest=\"identifier\" />" + 
        		"    <copyField source=\"uri\" dest=\"content\" />" + 
        		"    <copyField source=\"title\" dest=\"content\" />" + 
        		"    <copyField source=\"description\" dest=\"content\" />" + 
        		"    <defaultSearchField>content</defaultSearchField>" + 
        		"    <solrQueryParser defaultOperator=\"OR\" />" + 
        		"</schema>");

        Index index = m_indexes.get(repositoryName);
        assertNotNull(index);
        
        Search search = m_searches.get(repositoryName);
        assertNotNull(search);
        
        Document doc = index.newDocument();
        doc.addField("uri", "/doc/1");
        doc.addField("title", "Hello World Almanac");
        doc.addField("description", "We are mostly harmless, and greet you from this green and blue planet.");
        index.update(doc);
        
        doc = index.newDocument();
        doc.addField("uri", "/doc/2");
        doc.addField("title", "Book Of Words");
        doc.addField("description", "Never ever in your life will you have witnessed so many words in seemingly random order.");
        index.update(doc);

        SearchResponse response = search.select("*:*", 0, 10, null);
        assertNotNull("We should have gotten a search response", response);

        Collection<Document> results = response.getResults();
        assertNotNull("We should have gotten a search response with results", results);
        assertEquals(2, results.size());

        for (Document document : results) {
            for (String name : document.getFieldNames()) {
                for (Object value : document.getFieldValues(name)) {
                    System.out.println(name + "=" + value);
                }
            }
        }

        assertNotNull("We should have deleted a single document", index.delete("identifier:/doc/1"));

        response = search.select("*:*", 0, 10, null);
        assertNotNull("We should have gotten a search response", response);
        
        results = response.getResults();
        assertNotNull("We should have gotten a search response with results", results);
        assertEquals(1, results.size());

        assertNotNull("We should have deleted a single document", index.deleteById("/doc/2"));
        
        config.delete();
        
        int timeout = 50;
        while (m_searches.size() > 0 && timeout-- > 0) {
        	Thread.sleep(100);
        }
        assertTrue("Timed out waiting for search core to disappear after removing configuration.", timeout > 0);
        assertEquals(0, m_indexes.size());
    }

    public void testConfigureTwoCoresAndCheckLifeCycleAndSimpleUsage() throws Exception {
        String repoName1 = "test-core-1";
        String repoName2 = "test-core-2";
        
        Configuration c1 = configureCore(repoName1,
            "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
            "<schema name=\"core1\" version=\"1.2\">" + 
            "    <types>" + 
            "        <fieldType name=\"string\" class=\"solr.StrField\" sortMissingLast=\"true\" omitNorms=\"true\" />" + 
            "        <fieldType name=\"boolean\" class=\"solr.BoolField\" sortMissingLast=\"true\" omitNorms=\"true\" />" + 
            "        <fieldType name=\"uri\" class=\"solr.TextField\" positionIncrementGap=\"100\"/>" + 
            "    </types>" + 
            "    <fields>" + 
            "        <field name=\"uri\" type=\"uri\" stored=\"true\" indexed=\"true\" required=\"true\" />" + 
            "        <field name=\"title\" type=\"string\" stored=\"true\" indexed=\"true\" />" + 
            "        <field name=\"description\" type=\"string\" stored=\"true\" indexed=\"true\" />" + 
            "    </fields>" + 
            "    <uniqueKey>uri</uniqueKey>" + 
            "    <solrQueryParser defaultOperator=\"OR\" />" + 
            "</schema>");
        Configuration c2 = configureCore(repoName2, 
            "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + 
            "<schema name=\"core2\" version=\"1.2\">" + 
            "    <types>" + 
            "        <fieldType name=\"string\" class=\"solr.StrField\" sortMissingLast=\"true\" omitNorms=\"true\" />" + 
            "        <fieldType name=\"boolean\" class=\"solr.BoolField\" sortMissingLast=\"true\" omitNorms=\"true\" />" + 
            "        <fieldType name=\"uri\" class=\"solr.TextField\" positionIncrementGap=\"100\"/>" + 
            "    </types>" + 
            "    <fields>" + 
            "        <field name=\"uri\" type=\"uri\" stored=\"true\" indexed=\"true\" required=\"true\" />" + 
            "        <field name=\"foo\" type=\"string\" stored=\"true\" indexed=\"true\" />" + 
            "        <field name=\"bar\" type=\"string\" stored=\"true\" indexed=\"true\" />" + 
            "    </fields>" + 
            "    <uniqueKey>uri</uniqueKey>" + 
            "    <solrQueryParser defaultOperator=\"OR\" />" + 
            "</schema>");

        Index index1 = m_indexes.get(repoName1);
        assertNotNull(index1);

        Document doc = index1.newDocument();
        doc.addField("uri", "/doc/1");
        doc.addField("title", "Hello World Almanac");
        doc.addField("description", "We are mostly harmless, and greet you from this green and blue planet.");
        index1.update(doc);

        Index index2 = m_indexes.get(repoName2);
        assertNotNull(index2);
        
        doc = index2.newDocument();
        doc.addField("uri", "/doc/2");
        doc.addField("foo", "Book Of Words");
        doc.addField("bar", "Never ever in your life will you have witnessed so many words in seemingly random order.");
        index2.update(doc);

        Search search1 = m_searches.get(repoName1);
        assertNotNull(search1);

        SearchResponse response = search1.select("title:Hello*", 0, 10, null);
        assertNotNull("We should have gotten a search response", response);
        
        Collection<Document> results = response.getResults();
        assertNotNull("We should have gotten a search response with results", results);
        assertEquals(1, results.size());

        response = search1.select("foo:Book*", 0, 10, null);
        assertNotNull("We should have gotten a search response", response);
        assertTrue(response.getResults().isEmpty());

        Search search2 = m_searches.get(repoName2);
        assertNotNull(search2);

        response = search2.select("foo:Book*", 0, 10, null);
        assertNotNull("We should have gotten a search response", response);
        
        results = response.getResults();
        assertNotNull("We should have gotten a search response with results", results);
        assertEquals(1, results.size());

        for (Document document : results) {
            for (String name : document.getFieldNames()) {
                for (Object value : document.getFieldValues(name)) {
                    System.out.println(name + "=" + value);
                }
            }
        }
        
        c1.delete();
        int timeout = 50;
        while (m_searches.size() > 1 && timeout-- > 0) {
        	Thread.sleep(100);
        }
        assertTrue("Timed out waiting for first search core to disappear after removing configuration.", timeout > 0);
        assertEquals(1, m_indexes.size());

        c2.delete();
        timeout = 50;
        while (m_searches.size() > 0 && timeout-- > 0) {
        	Thread.sleep(100);
        }
        assertTrue("Timed out waiting for second search core to disappear after removing configuration.", timeout > 0);
        assertEquals(0, m_indexes.size());
    }
    
    protected void addIndex(Map<Object, Object> serviceProps, Index index) {
        String key = (String) serviceProps.get("repository");
        m_indexes.putIfAbsent(key, index);
    }
    
    protected void addSearch(Map<Object, Object> serviceProps, Search search) {
        String key = (String) serviceProps.get("repository");
        m_searches.putIfAbsent(key, search);
    }
    
    protected void removeIndex(Map<Object, Object> serviceProps, Index index) {
        String key = (String) serviceProps.get("repository");
        m_indexes.remove(key, index);
    }
    
    protected void removeSearch(Map<Object, Object> serviceProps, Search search) {
        String key = (String) serviceProps.get("repository");
        m_searches.remove(key, search);
    }

    @Override
    protected void setUp() throws Exception {
        m_dm = new DependencyManager(m_context);
        m_component = m_dm.createComponent()
            .setImplementation(this)
            .add(m_dm.createServiceDependency()
                .setService(ConfigurationAdmin.class)
                .setRequired(true)
            );
        m_dm.add(m_component);

        if (!m_semaphore.tryAcquire(5, TimeUnit.SECONDS)) {
            throw new Exception("Timed out waiting for required dependencies.");
        }
        m_semaphore.release();
    }

    protected void start() {
        m_semaphore.release();
    }

    protected void stop() {
        m_semaphore.acquireUninterruptibly();
    }

    @Override
    protected void tearDown() throws Exception {
        m_dm.remove(m_component);
        assertEquals(0, m_semaphore.availablePermits());
    }

    /**
     * @param repositoryName
     * @throws IOException
     * @throws InterruptedException
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private Configuration configureCore(String repositoryName, String... configs) throws IOException, InterruptedException, Exception {
        Dictionary props = new Properties();
        props.put(org.amdatu.search.Constants.REPOSITORY_NAME, repositoryName);
        if (configs != null) {
            if (configs.length > 0) {
                // First the schema is to be provided...
                props.put(org.amdatu.search.Constants.REPOSITORY_CONFIG_SCHEMA, configs[0]);
            }
        }

        Configuration config = m_configAdmin.createFactoryConfiguration(FACTORY_PID, null);
        config.update(props);

        m_component.add(m_dm
            .createServiceDependency()
            .setService(Search.class,
                "(&(" + Constants.OBJECTCLASS + "=" + Search.class.getName() + ")("
                    + org.amdatu.search.Constants.REPOSITORY_NAME + "=" + repositoryName + "))")
            .setCallbacks("addSearch", "removeSearch")
            .setInstanceBound(true)
            .setRequired(true));
        m_component.add(m_dm
            .createServiceDependency()
            .setService(Index.class,
                "(&(" + Constants.OBJECTCLASS + "=" + Index.class.getName() + ")("
                    + org.amdatu.search.Constants.REPOSITORY_NAME + "=" + repositoryName + "))")
            .setCallbacks("addIndex", "removeIndex")
            .setInstanceBound(true)
            .setRequired(true));

        if (!m_semaphore.tryAcquire(500, TimeUnit.SECONDS)) {
            throw new Exception("Timed out waiting for Search and Index services.");
        }

        m_semaphore.release();
        return config;
    }
}
