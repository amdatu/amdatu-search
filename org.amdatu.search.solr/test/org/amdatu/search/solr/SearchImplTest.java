/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr;

import static org.amdatu.search.Constants.ENRICHER_CONFIG;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_ENGINE;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_SCHEMA;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_STOPWORDS;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_SYNONYMS;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_URLWORDS;
import static org.amdatu.search.Constants.REPOSITORY_NAME;

import java.io.File;
import java.util.Properties;

import junit.framework.TestCase;

import org.amdatu.search.Document;
import org.amdatu.search.Search;
import org.amdatu.search.enricher.Enricher;
import org.amdatu.search.enricher.EnricherContext;
import org.amdatu.search.enricher.EnricherException;
import org.apache.commons.io.FileUtils;
import org.osgi.service.cm.ConfigurationException;

/**
 * Test case for {@link SearchImpl}.
 */
public class SearchImplTest extends TestCase {
	
	private String m_tmpDir;
	private SearchImpl m_search;
	
	/**
	 * @param name the name of this test case, can be <code>null</code>.
	 */
	public SearchImplTest(String name) {
		super(name);
	}

	/**
	 * Tests that calling an API method on an initialized {@link SearchImpl} works as expected.
	 */
	public void testInitializedObjectCanBeUsedOk() throws Exception {
		Properties props = getSolrConfiguration();
		
		m_search.updateSolrProperties(props); // should succeed.
		
		assertNotNull(m_search.newDocument()); // should succeed.
	}

	/**
	 * Tests that calling an API method on a {@link SearchImpl} on which {@link SearchImpl#removeRepository()} is called, causes an {@link IllegalStateException}.
	 */
	public void testRemovedRepositoryThrowsExceptionUponFurtherUsage() throws Exception {
		Properties props = getSolrConfiguration();
		
		m_search.updateSolrProperties(props); // should succeed.

		m_search.removeRepository(); // should succeed!

		try {
			m_search.newDocument();
			
			fail("Expected IllegalStateException!");
		} catch (IllegalStateException e) {
			// Ok; expected...
		}
	}

	/**
	 * Tests that calling an API method on an uninitialized {@link SearchImpl} causes an {@link IllegalStateException}.
	 */
	public void testUninitializedObjectThrowsException() throws Exception {
		try {
			m_search.newDocument();
			
			fail("Expected IllegalStateException!");
		} catch (IllegalStateException e) {
			// Ok; expected...
		}
	}

	/**
	 * Tests that calling a {@link SearchImpl#updateSolrProperties(java.util.Dictionary)} causes the server to be initialized.
	 */
	public void testUpdateSolrPropertiesInitializesServerOk() throws Exception {
		Properties props = getSolrConfiguration();

		m_search.updateSolrProperties(props);
	}

	/**
	 * Tests that calling a {@link SearchImpl#updateSolrProperties(java.util.Dictionary)} without a {@link Search#REPOSITORY_NAME} property fails with a {@link ConfigurationException}.
	 */
	public void testUpdateSolrPropertiesRequiresRepositoryName() throws Exception {
		Properties props = new Properties();
		
		try {
			m_search.updateSolrProperties(props);
			
			fail("Expected ConfiguredException!");
		} catch (ConfigurationException e) {
			// Ok; expected...
		}
	}

	/**
	 * Tests that calling a {@link SearchImpl#updateSolrProperties(java.util.Dictionary)} with a different {@link Search#REPOSITORY_NAME} property fails with a {@link ConfigurationException}.
	 */
	public void testUpdateSolrPropertiesRequiresRepositoryNameToRemainStable() throws Exception {
		Properties props = getSolrConfiguration();
		
		m_search.updateSolrProperties(props); // should succeed.
		
		props.put(REPOSITORY_NAME, "foo");

		try {
			m_search.updateSolrProperties(props); // should fail.
			
			fail("Expected ConfiguredException!");
		} catch (ConfigurationException e) {
			// Ok; expected...
		}
	}

	/**
	 * Tests that calling a {@link SearchImpl#updateSolrProperties(java.util.Dictionary)} twice causes the server to be initialized & updated.
	 */
	public void testUpdateSolrPropertiesUpdatesServerOk() throws Exception {
		Properties props = getSolrConfiguration();

		m_search.updateSolrProperties(props); // should succeed.
		m_search.updateSolrProperties(props); // should succeed.
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		m_tmpDir = new File(System.getProperty("java.io.tmpdir"), "solr-" + System.currentTimeMillis()).getAbsolutePath();

		m_search = new SearchImpl(m_tmpDir);
		m_search.setEnricher(new Enricher() {
			@Override
			public Document handle(Document document, String expression) throws EnricherException {
				return handle(document, expression, null);
			}

			@Override
			public Document handle(Document document, String expression, EnricherContext context) throws EnricherException {
				return null;
			}
			
			@Override
			public String replace(String expression, Document document) {
				return replace(expression, document, null);
			}
			
			@Override
			public String replace(String expression, Document document, String encodingScheme) {
				return null;
			}
		});
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	protected void tearDown() throws Exception {
		FileUtils.deleteQuietly(new File(m_tmpDir));

		super.tearDown();
	}
	
	/**
	 * @return a minimalistic Solr configuration, never <code>null</code>.
	 */
	private Properties getSolrConfiguration() {
		Properties props = new Properties();
		props.put(REPOSITORY_NAME, "test");
		props.put(REPOSITORY_CONFIG_SCHEMA, "<schema name=\"default\" version=\"1.2\"><types></types><fields></fields><solrQueryParser defaultOperator=\"OR\" /></schema>");
		props.put(REPOSITORY_CONFIG_ENGINE, "<config></config>");
		props.put(REPOSITORY_CONFIG_STOPWORDS, "");
		props.put(REPOSITORY_CONFIG_SYNONYMS, "");
		props.put(REPOSITORY_CONFIG_URLWORDS, "");
		props.put(ENRICHER_CONFIG, "<enrichers></enrichers>");
		return props;
	}
}
