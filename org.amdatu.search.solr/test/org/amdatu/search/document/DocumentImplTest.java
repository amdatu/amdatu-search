/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.search.document;

import static org.amdatu.search.Constants.*;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.amdatu.search.Document;

import junit.framework.TestCase;

/**
 * Test case for {@link DocumentImpl}.
 */
public class DocumentImplTest extends TestCase {

    /**
     * @param name the name of the test case.
     */
    public DocumentImplTest(String name) {
        super(name);
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#addField(java.lang.String, java.lang.Object)}.
     */
    public void testAddAndGetNamedFieldOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("bar", "foo");

        Collection<Object> values = doc.getFieldValues("bar");
        assertNotNull(values);
        assertEquals(1, values.size());
        assertTrue(values.contains("foo"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#addField(java.lang.Object)}.
     */
    public void testAddAndGetUnnamedFieldOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("foo");

        Collection<Object> values = doc.getFieldValues(UNNAMED_FIELD);
        assertNotNull(values);
        assertEquals(1, values.size());
        assertTrue(values.contains("foo"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#addField(java.lang.String, java.lang.Object, java.util.Map)}.
     */
    public void testAddNamedFieldWithAttributesOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("bar", "foo", new HashMap<String, String>());

        Collection<Object> values = doc.getFieldValues("bar");
        assertNotNull(values);
        assertEquals(1, values.size());
        assertTrue(values.contains("foo"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#addField(java.lang.Object, java.util.Map)}.
     */
    public void testAddUnnamedFieldWithAttributesOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField((Object) "foo", new HashMap<String, String>());

        Collection<Object> values = doc.getFieldValues(UNNAMED_FIELD);
        assertNotNull(values);
        assertEquals(1, values.size());
        assertTrue(values.contains("foo"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#equals(java.lang.Object)}.
     */
    public void testEqualsWithNullYieldsSameSemanticsAsIsEmpty() {
        DocumentImpl doc = new DocumentImpl();
        assertTrue(doc.equals(null));
        
        doc.addField("foo", "bar");
        assertFalse(doc.equals(null));
        
        doc.setField("foo", Collections.emptyList());
        assertTrue(doc.equals(null));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#equals(java.lang.Object)}.
     */
    public void testEqualsWithNotNullYieldsSameSemanticsAsGetDocumentWithString() {
        DocumentImpl doc = new DocumentImpl();
        assertFalse(doc.equals("^$"));

        doc.addField("foo", "bar");
        assertTrue(doc.equals("bar"));

        doc.setField("foo", Collections.emptyList());
        assertFalse(doc.equals("^$"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#getDocument(java.lang.String)}.
     */
    public void testGetDocumentWithConstantValueOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("foo", "bar");
        doc.addField("foo", "qux");
        doc.addField("foo", "quu");

        Document sub = doc.getDocument("qux");
        assertNotNull(sub);

        Collection<Object> fieldValues = sub.getFieldValues("foo");
        assertEquals(1, fieldValues.size());
        assertTrue(fieldValues.contains("qux"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#getDocument(java.lang.String)}.
     */
    public void testGetDocumentWithRegexOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("foo", "bar");
        doc.addField("foo", "qux");
        doc.addField("foo", "quu");

        Document sub = doc.getDocument("(qux|quu)");
        assertNotNull(sub);

        Collection<Object> fieldValues = sub.getFieldValues("foo");
        assertEquals(2, fieldValues.size());
        assertTrue(fieldValues.contains("qux"));
        assertTrue(fieldValues.contains("quu"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#getFieldNames()}.
     */
    public void testGetFieldNamesOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("quu");
        doc.addField("foo", "qux");
        doc.addField("bar", "qux");
        
        Collection<String> fieldNames = doc.getFieldNames();
        assertNotNull(fieldNames);
        
        assertEquals(3, fieldNames.size());
        assertTrue(fieldNames.contains("0"));
        assertTrue(fieldNames.contains("foo"));
        assertTrue(fieldNames.contains("bar"));
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#getFieldNames()}.
     */
    public void testGetFieldNamesOnEmptyDocumentOk() {
        DocumentImpl doc = new DocumentImpl();
        
        Collection<String> fieldNames = doc.getFieldNames();
        assertNotNull(fieldNames);
        
        assertEquals(0, fieldNames.size());
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#getFields()}.
     */
    public void testGetFieldsOnDocumentOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("test");
        
        assertNotNull(doc.getFields());
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#getFields()}.
     */
    public void testGetFieldsOnEmptyDocumentOk() {
        DocumentImpl doc = new DocumentImpl();
        assertNotNull(doc.getFields());
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#isEmpty()}.
     */
    public void testIsEmptyOk() {
        DocumentImpl doc = new DocumentImpl();
        assertTrue(doc.isEmpty());
        
        doc.addField("foo", "bar");
        assertFalse(doc.isEmpty());
        
        doc.setField("foo", Collections.emptyList());
        assertTrue(doc.isEmpty());
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#removeField(java.lang.String)}.
     */
    public void testRemoveNamedFieldOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("bar", "foo");

        Collection<Object> values = doc.getFieldValues("bar");
        assertNotNull(values);
        assertEquals(1, values.size());
        
        doc.removeField("bar");
        
        values = doc.getFieldValues("bar");
        assertNull(values);
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#removeField(java.lang.String)}.
     */
    public void testRemoveUnnamedFieldOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.addField("foo");

        Collection<Object> values = doc.getFieldValues(UNNAMED_FIELD);
        assertNotNull(values);
        assertEquals(1, values.size());
        
        doc.removeField(UNNAMED_FIELD);
        
        values = doc.getFieldValues(UNNAMED_FIELD);
        assertNull(values);
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#setField(java.lang.String, java.lang.Object)}.
     */
    public void testSetNamedFieldOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.setField("foo", "bar");

        assertTrue(doc.getFieldValues("foo").contains("bar"));
        
        List<Object> newValue = Collections.emptyList();
        
        doc.setField("foo", newValue);

        assertTrue(doc.getFieldValues("foo").isEmpty());
    }

    /**
     * Test method for {@link org.amdatu.search.document.DocumentImpl#setField(java.lang.String, java.lang.Object, java.util.Map)}.
     */
    public void testSetNamedFieldWithAttributesOk() {
        DocumentImpl doc = new DocumentImpl();
        doc.setField("foo", "bar", new HashMap<String, String>());

        assertTrue(doc.getFieldValues("foo").contains("bar"));
        
        List<Object> newValue = Collections.emptyList();
        
        doc.setField("foo", newValue, new HashMap<String, String>());

        assertTrue(doc.getFieldValues("foo").isEmpty());
    }
}
