/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.document;

import java.util.Map;


public class Field extends DocumentImpl {
	private Map<String, String> m_attributes;
	private Object m_value;
	
	public Field(Object value) {
		setValue(value);
	}
	
	public Field(Object value, Map<String, String> attributes) {
		this(value);
		setAttributes(attributes);
	}

	public Object getValue() {
		return m_value;
	}

	// TODO consider making the method private and/or the field final
	public void setValue(Object value) {
		m_value = value;
	}

    // TODO consider making the method private and/or the field final
	public void setAttributes(Map<String, String> attributes) {
		m_attributes = attributes;
	}

	public Map<String, String> getAttributes() {
		return m_attributes;
	}

	public String toString() {
	    // TODO can this be null?
		return m_value.toString();
	}
}
