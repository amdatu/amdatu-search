/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.document;

import java.util.Collection;
import java.util.Map;

import org.amdatu.search.document.Field;

/**
 * Extends {@link DocumentImpl} and adds support for attributes.
 */
public class DocumentWithAttrImpl extends DocumentImpl {
	@Override
	public void addField(String name, Object value, Map<String, String> attributes) {
		if (value instanceof Collection) {
			for (Object v : (Collection<?>) value) {
				addField(name, v, attributes);
			}
		}
		else {	
			super.addField(name, new Field(value, attributes));
		}
	}

	@Override
	public void addField(String name, Object value) {
		addField(name, value, null);
	}		
}
