/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.amdatu.search.Constants;
import org.amdatu.search.Document;

/**
 * Provides an implementation of {@link Document}.
 */
public class DocumentImpl implements Document {
    protected final Map<String, Collection<Object>> m_fields = new HashMap<String, Collection<Object>>();

    public void addField(Object value) {
        addField(Constants.UNNAMED_FIELD, value);
    }

    public void addField(Object value, Map<String, String> attributes) {
        addField(Constants.UNNAMED_FIELD, value, attributes);
    }

    public void addField(String name, Object value, Map<String, String> attributes) {
        // this implementation does not have attributes, so we simply ignore them here
        addField(name, value);
    }

    public void addField(String name, Object value) {
    	if(value == null) {
    		return;
    	}
    	
        List<Object> fieldValues = (List<Object>) getFieldValues(name);
        if (fieldValues == null) {
            fieldValues = new ArrayList<Object>();
            m_fields.put(name, fieldValues);
        }

        if (value instanceof Collection) {
            for (Object v : (Collection<?>) value) {
                // Recursively add the given value (might be a complex value as well!)
                addField(name, v);
            }
        }
        else {
            fieldValues.add(value);
        }
    }

    public Object removeField(String name) {
        return m_fields.remove(name);
    }

	public String getId() {
	    Collection<Object> fieldValues = getFieldValues("identifier");
	    Iterator<Object> iterator = fieldValues.iterator();
	    if (iterator.hasNext()) {
            return (String) iterator.next();
	    }
        throw new IllegalStateException("Could not obtain identifier for document.");
	}
	
    public Collection<String> getFieldNames() {
        return m_fields.keySet();
    }

    public Collection<Object> getFieldValues(String name) {
        return m_fields.get(name);
    }

    public Map<String, Collection<Object>> getFields() {
        return m_fields;
    }

    public void setField(String name, Object value) {
        removeField(name);
        addField(name, value);
    }

    public void setField(String name, Object value, Map<String, String> attributes) {
        setField(name, value);
    }

    public Document getDocument(String pattern) {
        Document retval = null;
        Pattern p = Pattern.compile(pattern);
        Matcher matcher;
        for (String name : getFieldNames()) {
            for (Object value : getFieldValues(name)) {
                if (value != null) {
                    matcher = p.matcher(value.toString());
                    if (matcher.matches()) {
                        if (retval == null) {
                            retval = newDocument();
                        }
                        retval.addField(name, value);
                    }
                }
            }
        }
        return retval;
    }

    public Document newDocument() {
        Document retval = null;
        try {
            retval = (Document) getClass().newInstance();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return retval;
    }

    // TODO review if this is always consistent with the default hashCode() implementation
    @Override
    public boolean equals(Object obj) {
        boolean retval = false;
        if (obj == null) {
            retval = isEmpty();
        }
        else {
            retval = getDocument(obj.toString()) != null;
        }
        return retval;
    }

    public boolean isEmpty() {
        for (String name : getFieldNames()) {
            Collection<Object> values = getFieldValues(name);
            if ((values != null) && !values.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        for (String name : getFieldNames()) {
            result.append(name);
            result.append("=[");
            for (Object value : getFieldValues(name)) {
                result.append(value.toString());
                result.append(",");
            }
            result.append("]");
        }
        result.append("]");
        return result.toString();
    }
}
