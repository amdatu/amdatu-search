/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.request;

import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.solr.common.params.SolrParams;

public class TermClauseField {
	private Occur m_occur = Occur.SHOULD;
	private float m_boost = 1.0f;
	private Object m_term;
	private SolrParams m_localParams;
	
	public TermClauseField(Object term) {
		m_term = term;
	}
	
	public TermClauseField(Object term, SolrParams localParams) {
		m_term = term;
		m_localParams = localParams;		
	}

	public TermClauseField(Occur occur, Object term, float boost) {
		m_occur = occur;
		m_term = term;
		m_boost = boost;
	}

	public TermClauseField(Occur occur, Object term, float boost, SolrParams localParams) {
		m_occur = occur;
		m_term = term;
		m_boost = boost;
		m_localParams = localParams;
	}
	
	/**
	 * @return the occur
	 */
	public Occur getOccur() {
		return m_occur;
	}

	/**
	 * @param occur the occur to set
	 */
	public void setOccur(Occur occur) {
		m_occur = occur;
	}

	/**
	 * @return the boost
	 */
	public float getBoost() {
		return m_boost;
	}

	/**
	 * @param boost the boost to set
	 */
	public void setBoost(float boost) {
		m_boost = boost;
	}

	/**
	 * @return the text
	 */
	public Object getTerm() {
		return m_term;
	}

	/**
	 * @param text the text to set
	 */
	public void setTerm(Object term) {
		m_term = term;
	}

	/**
	 * @return the tag
	 */
	public SolrParams getLocalParams() {
		return m_localParams;
	}

	/**
	 * @param text the text to set
	 */
	public void setLocalParams(SolrParams localParams) {
		m_localParams = localParams;
	}

	public String toQueryString() {
		String retval = m_occur + m_term.toString().trim();
		if (m_boost != 1.0f) {
			retval += "^" + m_boost;
		}
		return retval;
	}
	
	public String toString() {
		return (m_term == null) ? "" : m_term.toString();
	}
}
