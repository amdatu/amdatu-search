/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.request;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.amdatu.search.Document;
import org.amdatu.search.document.DocumentImpl;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;


public class SolrRequestDocumentImpl extends DocumentImpl {
	public SolrRequestDocumentImpl() {
		super();
	}
	
	@Override
	public void addField(String name, Object value, Map<String,String> attributes) {
		if (value instanceof Collection) {
			for (Object v : (Collection<?>) value) {
				addField(name, v, attributes);
			}
		} else {
			if (name.startsWith("q.") || name.startsWith("fq.")) {
				Occur occur = attributes.containsKey("occur") ? Occur.valueOf(attributes.get("occur")) : Occur.SHOULD;
				float boost = attributes.containsKey("boost") ? Float.valueOf(attributes.get("boost")) : 1.0f;
				if (value instanceof TermClauseField) {
					((TermClauseField) value).setOccur(occur);
					((TermClauseField) value).setBoost(boost);
					super.addField(name, value);
				} else {
					super.addField(name, new TermClauseField(occur, value.toString(), boost));
				}				
			} else {
				super.addField(name, value, attributes);				
			}
		}
	}

	@Override
	public void addField(String name, Object value) {
		if (value instanceof Collection) {
			for (Object v : (Collection<?>)value) {
				addField(name, v);
			}
		} else {	
			if (name.startsWith("q.") || name.startsWith("fq.")) {
				if (value instanceof TermClauseField) {
					super.addField(name, value);
				} else {
					super.addField(name, new TermClauseField(value.toString()));
				}
			} else {
				super.addField(name, value);								
			}
		}
	}

	public Document getDocument(String pattern) {
		Document retval = null;

		String compareTo = pattern;

		BooleanClause.Occur occur = Occur.SHOULD;
		if (compareTo.startsWith("+") || compareTo.startsWith("-")) {
			compareTo = compareTo.substring(1);
			occur = Occur.valueOf(compareTo.substring(0, 1));
		}

		Pattern p = Pattern.compile(compareTo);
		Matcher matcher;
		for(String name : getFieldNames()) {
			for(Object value : getFieldValues(name)) {
				if (value != null) {
					matcher = p.matcher(value.toString());
					if (matcher.matches()) {
						if (((TermClauseField) value).getOccur().equals(occur)) {
							if (retval == null) {
								retval = newDocument();
							}
							retval.addField(name, value);
						}
					} 
				}
			}
		}

		return retval;
	}
}
