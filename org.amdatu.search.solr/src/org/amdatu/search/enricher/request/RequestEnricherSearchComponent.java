/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.request;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.amdatu.search.enricher.EnricherException;
import org.amdatu.search.enricher.impl.AbstractEnricherSearchComponent;
import org.amdatu.search.enricher.impl.NamedListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.solr.common.params.MapSolrParams;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.schema.IndexSchema;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QueryParsing;
import org.apache.solr.search.SortSpec;

/**
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class RequestEnricherSearchComponent extends AbstractEnricherSearchComponent {
    public static final String EXPRESSION = "request.enricher.expression";
    public static final String SOURCE = "request.enricher.source";
    public static final String NAME = "request.enricher.name";
    public static final String TYPE = "request.enricher.type";
    
	/**
	 * {@inheritDoc}
	 */
	public void process(ResponseBuilder rb) throws IOException {
    	String qParserName = "lucene";
		try {
		    SolrParams params = rb.req.getParams();
		    
		    if (params == null)  {
		        return;
		    }
	        
	    	String expression = params.get(EXPRESSION);
		    if (expression == null) {
		        return;
		    }

            IndexSchema schema = rb.req.getSchema();
            Map<Object, Object> context = rb.req.getContext();

	    	SolrRequestDocumentImpl document = new SolrRequestDocumentImpl();
	    	
	    	// EXTRACT (FILTER)QUERIES WITH TAGS
	    	Map<Query,String> tagsByQuery = new HashMap<Query, String>();
	     
            Map<String,Collection<Object>> tags = (Map<String, Collection<Object>>) context.get("tags");
            if (tags != null) {
	        	for (String tag : tags.keySet()) {
		            Object taggedQueries = tags.get(tag);
		            if (taggedQueries instanceof Collection) {
		            	for (QParser taggedQParser : (Collection<QParser>)taggedQueries) {
	            			tagsByQuery.put(((QParser)taggedQParser).getQuery(), tag);
		            	}
		            }
	        	}
	        }
            
	    	// EXTRACT (FILTER)QUERIES
	    	try {
	    		// ADD QUERY
	    		Query q = rb.getQuery();
	    		String t = tagsByQuery.get(q);
		    	addQuery(schema, tagsByQuery, document, "q.", rb.getQuery(), BooleanClause.Occur.SHOULD, ((t != null) ? createLocalParams("tag", t) : null));
	    		
	    		// ADD FILTERQUERIES
				List<Query> filters = rb.getFilters();
				if (filters != null) {
					for (Query fq : filters) {
						String tfq = tagsByQuery.get(fq);
				    	addQuery(schema, tagsByQuery, document, "fq.", fq, BooleanClause.Occur.SHOULD, ((tfq != null) ? createLocalParams("tag", tfq) : null));
					}
				}
	    	} catch (EnricherException ee) {
	    		// skip enriching
	    		return;
	    	}
	    	
	    	// EXTRACT PARAMS
	    	for (Iterator<String> it = params.getParameterNamesIterator(); it.hasNext(); ) {
	    		String name = it.next();
	    		if (!"q".equalsIgnoreCase(name) && !"fq".equalsIgnoreCase(name)) {
	    			if (document.getFieldValues(name) == null) {
		    			String[] values = params.getParams(name);
		    			for (String value : values) {
		    				SolrParams localParams = QueryParsing.getLocalParams(value, params);
		    				if (localParams != null) {
		    					document.addField(name, new TermClauseField(localParams.get("v"), localParams));
		    				} else {
		    					document.addField(name, new TermClauseField(value));
		    				}
		    			}
		    		}
	    		}
	    	}
	    	
	    	// EXECUTE ENRICHER
	    	document = (SolrRequestDocumentImpl) executeEnricher(params.get(SOURCE), params.get(TYPE), params.get(NAME), expression, document);

	    	// REMOVE OLD TAGS TO ENSURE PROPER TAGGING AND EXCLUSION DURING FACETTING
			context.remove("tags");

		    // ADD PARAMETERS
    		ModifiableSolrParams newParams = new ModifiableSolrParams();
			for (String name : document.getFieldNames()) {
				if (!name.startsWith("q.") && !name.startsWith("fq.")) {
					for (Object value : (Collection<Object>)document.getFieldValues(name)) {
						String paramValue = null;
						if (value instanceof TermClauseField) {
							paramValue = localParamsToString(((TermClauseField)value).getLocalParams()) + value.toString();
						} else {
							paramValue = value.toString();									
						}
						newParams.add(name, paramValue);
					}
				}
				
			}
    		rb.req.setParams(newParams);
    		
    		if (newParams.get("sort") != null) {
    			String sort = newParams.get("sort");
    			SortSpec sp = new SortSpec(QueryParsing.parseSort(sort, rb.req), rb.getSortSpec().getOffset(), rb.getSortSpec().getCount());
    			rb.setSortSpec(sp);
    		}

	    	// GENERATE NEW QUERY
	    	String queryString = "";
	    	
			for (String name : document.getFieldNames()) {
				if (name.startsWith("q.")) {
					String fieldName = name.replaceAll("q.", "");
					for (Object value : document.getFieldValues(name)) {
						TermClauseField tcf = (TermClauseField)value;
						queryString += " " + localParamsToString(tcf.getLocalParams()) + tcf.getOccur() + (fieldName.isEmpty() ? "" : (fieldName+":")) +  tcf.getTerm().toString().trim()+ ((tcf.getBoost()==1.0) ? "" : ("^" + tcf.getBoost()));
					}
				}
			}

			if (StringUtils.isEmpty(queryString)) {
				queryString="*:*";
			}

			QParser qParser = QParser.getParser(queryString.trim(), qParserName, rb.req);
		    Query newQuery = qParser.getQuery();
			rb.setQuery(newQuery);

	    	// GENERATE NEW FILTERED QUERY
    		List<Query> filteredQueries = new ArrayList<Query>();

    		HashMap<String, String> filteredQueryStringsByLocalParams = new HashMap<String, String>();
    		HashMap<String, String> filteredQueryStringsByFieldName = new HashMap<String, String>();

    		for (String name : document.getFieldNames()) {
				if (name.startsWith("fq.")) {
					String fieldName = name.replaceAll("fq.", "");
					for (Object value : document.getFieldValues(name)) {
						TermClauseField tcf = (TermClauseField) value;
						String filteredQueryString = tcf.getOccur() + (fieldName.isEmpty() ? "" : (fieldName+":")) +  tcf.getTerm().toString().trim()+ ((tcf.getBoost()==1.0) ? "" : ("^" + tcf.getBoost()));
						
						String localParamString = localParamsToString(tcf.getLocalParams());
						if ((localParamString != null) && !localParamString.isEmpty()) {
							// DO LOCAL PARAMS LATER
							String f1 = filteredQueryStringsByLocalParams.get(localParamString);
							if (f1 != null) {
								filteredQueryString += " " + f1;
							}
							
							filteredQueryStringsByLocalParams.put(localParamString, filteredQueryString);									
						} else {
							String f2 = filteredQueryStringsByFieldName.get(fieldName);
							if (f2 != null) {
								filteredQueryString += " " + f2;
							}
							filteredQueryStringsByFieldName.put(fieldName, filteredQueryString);									
						}
					}	
				}
			}

    		for (Map.Entry<String, String> entrySet : filteredQueryStringsByLocalParams.entrySet()) {
    			String f = entrySet.getKey() + "(" + entrySet.getValue() + ")";
				QParser fqParser = QParser.getParser(f.trim(), qParserName, rb.req);
		    	Query filteredQuery = fqParser.getQuery();
		    	filteredQueries.add(filteredQuery);
	    	}

    		for (Map.Entry<String, String> entrySet : filteredQueryStringsByFieldName.entrySet()) {
				QParser fqParser = QParser.getParser(entrySet.getValue().trim(), qParserName, rb.req);
		    	Query filteredQuery = fqParser.getQuery();
		    	filteredQueries.add(filteredQuery);
	    	}

		    if (!filteredQueries.isEmpty()) {
				rb.setFilters(filteredQueries);
			}
		    
		    // GENERATE RESPONSE
			NamedList responseList = null;
			for (String name : document.getFieldNames()) {
				if (name.startsWith("response/")) {
					Collection<Object> values = document.getFieldValues(name);
					if ((values!=null) && (values.size() > 0)) {
						ArrayList<String> valueList =  new ArrayList<String>();
						for (Object value : values) {
							valueList.add(value.toString());
						}
						responseList = NamedListUtils.mergeNamedLists(responseList, NamedListUtils.createNamedList(name.replaceFirst("response/", ""), valueList.toArray(new String[0])));
					}
				}
			}
			
			if (responseList != null) {
				for (int i = 0; i < responseList.size(); i++) {
					String name = responseList.getName(i);
					rb.rsp.add(name, responseList.get(name));
				}
			}
		} catch (ParseException e) {
			throw new IOException(e);
        } catch (EnricherException e) {
            throw new IOException(e);
        }
	}
	
	private void addQuery(IndexSchema schema, Map<Query,String> tagsByQuery, SolrRequestDocumentImpl document, String prefix, Query query, BooleanClause.Occur occur, SolrParams localParams) throws EnricherException {
		String field = prefix;
		TermClauseField value = null;
		float boost = query.getBoost();

		// BOOLEANQUERY
		if (query instanceof BooleanQuery) {
			BooleanQuery qry = (BooleanQuery) query;
			String tg = tagsByQuery.get(qry);
			for (BooleanClause clause : qry.getClauses()) {
				addQuery(schema, tagsByQuery, document, prefix, clause.getQuery(), (occur != Occur.MUST_NOT ? clause.getOccur() : occur), ((tg != null) ? createLocalParams("tag", tg): null));
			}
			return;
		} else if (query instanceof TermQuery) {
		    // TERMQUERY
			TermQuery qry = (TermQuery) query;
			String fieldName = qry.getTerm().field();
			String fieldValue = qry.getTerm().text().trim();
			
			field += fieldName;
			
			if (schema.getFieldType(fieldName).getClass().getName().contains("DateField")) {
				fieldValue = "'" + fieldValue.substring(0, 18) + ".000Z'";
			}
			
			value = new TermClauseField(occur, fieldValue, boost, localParams);
		} else if (query instanceof PhraseQuery) {
		    // PHRASEQUERY
			field += ((Term) ((PhraseQuery) query).getTerms()[0]).field();
			value = new TermClauseField(occur, query.toString().replaceFirst("^[^\\[]*:","").trim(), boost, localParams);
		} else if (query instanceof TermRangeQuery) {
		    // RANGEQUERY
			TermRangeQuery qry = (TermRangeQuery)query;
			
			String fieldName = qry.getField();
			field += fieldName;
			
			String lower = qry.getLowerTerm();
			String upper = qry.getUpperTerm();
			if (schema.getFieldType(fieldName).getClass().getName().contains("DateField")) {
				if (lower != null) {
					lower = lower.substring(0, 18) + ".000Z";
				} else {
					lower = "*";
				}
				
				if (upper != null) {
					upper = upper.substring(0, 18) + ".000Z";
				} else {
					upper = "*";
				}
			}

			value = new TermClauseField(occur, ((qry.includesLower()?"[":"{") + lower + " TO " + upper + (qry.includesUpper()?"]":"}")), boost, localParams);
		} else {
		    // OTHER
			value = new TermClauseField(occur, query.toString().trim(), boost, localParams);
		}
		document.addField(field, value);
	}
	
	private String localParamsToString(SolrParams localParams) {
		String retval = "";
		
		if (localParams != null) {
			retval = "{!" + ((localParams.get("type") != null) ? (localParams.get("type") + " ") : "");
			Iterator<String> it = localParams.getParameterNamesIterator();
			while (it.hasNext()) {
				String p = it.next();
				if (!"type".equalsIgnoreCase(p) && !"v".equalsIgnoreCase(p)) {
					retval += p + "=" + localParams.get(p) + " ";											
				}
			}
			retval = retval.trim() + "}";			
		}
		return retval;
	}
	
	private SolrParams createLocalParams(String name, String value) {
		Map<String,String> localParamsMap = new HashMap<String,String>();
		localParamsMap.put(name, value);
		return new MapSolrParams(localParamsMap);
	}
}
