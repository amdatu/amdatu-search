/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.response;

import java.io.IOException;

import org.amdatu.search.enricher.EnricherException;
import org.amdatu.search.enricher.impl.AbstractEnricherSearchComponent;
import org.amdatu.search.enricher.impl.NamedListUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.search.DocIterator;
import org.apache.solr.search.DocList;
import org.apache.solr.search.SolrIndexReader;
import org.apache.solr.util.SolrPluginUtils;


public class ResponseEnricherSearchComponent extends AbstractEnricherSearchComponent {
	public static final String EXPRESSION = "response.enricher.expression";
	public static final String SOURCE = "response.enricher.source";
	public static final String NAME = "response.enricher.name";
	public static final String TYPE = "response.enricher.type";
	
	@SuppressWarnings("rawtypes")
	public void process(ResponseBuilder rb) throws IOException {		
		try {
			SolrParams params = rb.req.getParams();
			if (params == null) {
			    return;
			}
		
			String expression = params.get(EXPRESSION);
			if (expression != null) {
			    return;
			}

			NamedList responseList = null;
			SolrDocumentList responseDocumentList = new SolrDocumentList();
			
			SolrIndexReader reader = rb.req.getSearcher().getReader();
			
			DocList docList = rb.getResults().docList;
			for (DocIterator it = docList.iterator(); it.hasNext();) {
				SolrResponseDocumentImpl document = new SolrResponseDocumentImpl(reader.document(it.nextDoc()));

				document = (SolrResponseDocumentImpl) executeEnricher(params.get(SOURCE), params.get(TYPE), params.get(NAME), expression, document);
	        	document.addField("score", it.score());

	        	SolrDocument responseDocument = new SolrDocument();
				for (String name : document.getFieldNames()) {
					if (name.startsWith("response/")) {
						responseList = NamedListUtils.mergeNamedLists(responseList, 
											NamedListUtils.createNamedList(name.replaceFirst("response/", ""), 
													document.getFieldValues(name).toArray(new String[0]))); // FIXME use list?!
					} else {
						responseDocument.addField(name, document.getFieldValues(name));
					}
				}
				responseDocumentList.add(responseDocument);
			}

			if (!responseDocumentList.isEmpty()) {
	        	responseDocumentList.setMaxScore(docList.maxScore());
	        	responseDocumentList.setNumFound(docList.matches());
				SolrPluginUtils.addOrReplaceResults(rb.rsp, responseDocumentList);
			}

			if (responseList != null) {
				for (int i = 0; i < responseList.size(); i++) {
					String name = responseList.getName(i);
					rb.rsp.add(name, responseList.get(name));
				}
			}
        } catch (EnricherException e) {
            throw new IOException(e);
        }
	}
}
