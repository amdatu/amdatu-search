/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.response;

import java.util.Collection;
import java.util.Map;

import org.amdatu.search.document.DocumentImpl;
import org.apache.lucene.document.Fieldable;
import org.apache.solr.common.SolrDocument;

public class SolrResponseDocumentImpl extends DocumentImpl {
	protected SolrDocument document;
	
	public SolrResponseDocumentImpl() {
		document = new SolrDocument();
	}
	
	public SolrResponseDocumentImpl(SolrDocument document) {
		this.document = document;
	}
	
	public SolrResponseDocumentImpl(org.apache.lucene.document.Document document) {
		this.document = new SolrDocument(); 
		for (Fieldable field : document.getFields()) {
			addField(field.name(), field.stringValue());
		} 	
	} 

	@Override
	public Collection<String> getFieldNames() {
		return(document.getFieldNames());
	}

	@Override
	public Collection<Object> getFieldValues(String name) {
		return(document.getFieldValues(name));
	}
			
	@Override
	public void addField(String name, Object value, Map<String, String> attributes) {
		addField(name, value);
	}

	@Override
	public void addField(String name, Object value) {
		document.addField(name, value);
	}

	@Override
	public Object removeField(String name) {
		Collection<Object> retval = (Collection<Object>)getFieldValues(name);
		if(retval!=null) {
			document.remove(name);
		}
		return(retval);
	}

	public SolrDocument getSolrDocument() {
		return(document);
	}
}
