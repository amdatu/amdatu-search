/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.impl;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.amdatu.search.Document;
import org.amdatu.search.enricher.EnricherContext;
import org.amdatu.search.enricher.EnricherException;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;


/**
 * Provides an enricher with an pipe-and-filter style filtering mechanism.
 */
public class PipeAndFilterEnricherImpl extends AbstractEnricherImpl {
    
	/**
	 * SAX handler for parsing the enricher XML file.
	 */
	public class EnricherSAXHandler extends DefaultHandler {
		private boolean m_execute;
	    private boolean m_executeElse;
	    private Document m_testValue;
	    private String m_iteratorFieldName;
	    private Document m_document;
	    private String m_expression;
	    private boolean m_isMultiValued;
	    private Map<String,String> m_attributes;
	    private Locator m_locator;
	    
	    private final Stack<Boolean> m_executeStack;
	    private final Stack<Boolean> m_executeElseStack;
	    private final Stack<Document> m_testValueStack;
	    private final Stack<String> m_iteratorFieldNameStack;
	    private final PipeAndFilterEnricherSAXFilter m_filter;
	        
	    /**
	     * Creates a new {@link EnricherSAXHandler} instance.
	     * 
	     * @throws SAXException in case creation of the embedded XML factory/reader failed.
	     */
		public EnricherSAXHandler() throws SAXException {
		    m_executeStack = new Stack<Boolean>();
		    m_executeElseStack = new Stack<Boolean>();
		    m_testValueStack = new Stack<Document>();
		    m_iteratorFieldNameStack = new Stack<String>();
		    
			XMLReader reader = XMLReaderFactory.createXMLReader();
		    m_filter = new PipeAndFilterEnricherSAXFilter();
	        m_filter.setParent(reader);
			m_filter.setContentHandler(this);
			
			m_execute = true;
			m_executeElse = false;
	    }
		
        /**
         * {@inheritDoc}
         */
		public void characters(char ch[], int start, int length) {
			m_expression += new String(ch,start, length).replaceAll("[\n\t]", " ");
	    }
		
        /**
         * {@inheritDoc}
         */
		public void endElement(String uri, String localName, String qName) throws SAXException {	
			try {
				String nameAttribute = m_attributes.get("name");
				if (!StringUtils.isEmpty(nameAttribute)) {
					nameAttribute = replace(nameAttribute, m_document, "UTF-8");
				}
				
				// ADD-FIELD
				if (m_execute && "add-field".equalsIgnoreCase(qName)) {	
					EnricherContext context = new EnricherContextImpl(m_attributes);

					Document sourceValue = m_document;
					String sourceAttribute = m_attributes.get("source");
					if (!StringUtils.isEmpty(sourceAttribute)) {
						sourceValue = executeEnricher(sourceAttribute, m_document, m_document, sourceAttribute, context);
					}
					
					if (m_attributes.get("type")!=null) {
						if (m_expression.contains("::")) {
							Document expressionDocument = executeEnricher(m_expression, m_document, m_document, m_expression, context);
							if(!expressionDocument.isEmpty()) {
								String name = (String)(expressionDocument.getFieldNames().toArray())[0]; // FIXME use List?
								m_expression = ((expressionDocument.getFieldValues(name).toArray())[0]).toString(); // FIXME use List?
							}
						}
					} else {
						m_attributes.put("type", m_expression);
					}

					Document value = executeEnricher(m_attributes.get("type"), m_document, sourceValue, m_expression, context);
					Collection<Object> values = null;
					if (!value.isEmpty()) {
                        if (!StringUtils.isEmpty(nameAttribute)) {
							// GET UNNAMED VALUE
							values = value.getFieldValues("0");
							if ((values == null) || values.isEmpty()) {
								values = value.getFieldValues(nameAttribute);
							} 
							
							if (!m_isMultiValued) {
		                        Object v = values.toArray()[0]; // FIXME use List?
								values = new ArrayList<Object>(1);
								values.add(v);

								m_document.removeField(nameAttribute);
							}
							
							m_document.addField(nameAttribute, values, m_attributes);
						} else {
							for (String name : value.getFieldNames()) {		
								values = value.getFieldValues(name);
								if (!m_isMultiValued) {
			                        Object v = values.toArray()[0]; // FIXME use List?
									values = new ArrayList<Object>(1);
									values.add(v);

									m_document.removeField(name);
								}

								m_document.addField(name, values, m_attributes);
							}
						}				
					}
				} else			
	
				// REMOVE-FIELD
				if (m_execute && "remove-field".equalsIgnoreCase(qName)) {
					m_document.removeField(nameAttribute);
				} else
			
				// REJECT-DOCUMENT
				if (m_execute && "reject-document".equalsIgnoreCase(qName)) {
					String message = replace(m_expression, m_document);
					throw new EnricherException(message);
				} else
	
				// IF-THEN-ELSE SWITCH-CASE-ELSE SELECT-DO
				if ( "if".equalsIgnoreCase(qName) || "then".equalsIgnoreCase(qName) || "else".equalsIgnoreCase(qName) || 
					 "switch".equalsIgnoreCase(qName) || "case".equalsIgnoreCase(qName) ||
					 "select".equalsIgnoreCase(qName) || "do".equalsIgnoreCase(qName)	) 
				{
					if (!m_executeStack.empty()) {
						m_execute = m_executeStack.pop();
						m_executeElse = m_executeElseStack.pop();
						m_testValue = m_testValueStack.pop();
						m_iteratorFieldName = m_iteratorFieldNameStack.pop();
					}
				} 

				// DO
				if ("do".equalsIgnoreCase(qName)) {
					if (m_iteratorFieldName!=null) {
						m_document.removeField(m_iteratorFieldName);
					}
				} 

				m_expression = null;
			} catch(EnricherException ee) {
				throw new SAXException(ee.getMessage());
			} catch(Exception e) {
				throw new SAXException("Problem in EnricherSAXHandler::endElement(" + uri + ", " + qName + ", " + qName + ") at line " + m_locator.getLineNumber() + " and column " + m_locator.getColumnNumber(), e);
			}
		}	

		/**
		 * Handles the given document and expression.
		 * 
		 * @param document the document to enrich, cannot be <code>null</code>;
		 * @param expression the expression to use in this enricher, cannot be <code>null</code>;
		 * @param context the enricher context, cannot be <code>null</code>.
		 * @return the enriched document, cannot be <code>null</code>.
		 * @throws EnricherException in case the enriching failed (at any stage).
		 */
		public Document handle(Document document, String expression, EnricherContext context) throws EnricherException {
			try {
				this.m_document = document;
				
			    InputSource configuration = null;
			    if(expression.contains("<")) {
					configuration = new InputSource(new ByteArrayInputStream(expression.getBytes("UTF-8")));
		        } else {
					configuration = new InputSource(new FileInputStream(expression.replaceAll("#.*","")));
					if(expression.contains("#")) {
						m_filter.setName(expression.replaceAll("[^#]*#",""));		
					}
			    }
				m_filter.parse(configuration);
			} catch (Exception e) {
				throw new EnricherException(e.getMessage(), e, document);
			}
			return document;
		}
		
        /**
         * {@inheritDoc}
         */
		public void setDocumentLocator(Locator locator) {
	        this.m_locator = locator;
	    }
		
		/**
		 * {@inheritDoc}
		 */
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {	
			try {
				m_attributes = fromAttributes(attributes);
				
				EnricherContext context = new EnricherContextImpl(m_attributes);
				
				m_expression = new String();
				
				String isMultiValuedString = m_attributes.get("multiValued");
				m_isMultiValued = (!StringUtils.isEmpty(isMultiValuedString) && StringUtils.contains(isMultiValuedString, "true"));
				
				if ("if".equalsIgnoreCase(qName) || "then".equalsIgnoreCase(qName) || "else".equalsIgnoreCase(qName) || "switch".equalsIgnoreCase(qName) || "case".equalsIgnoreCase(qName)) {
					m_executeStack.push(m_execute);
					m_executeElseStack.push(m_executeElse);
					m_testValueStack.push(m_testValue);
					m_iteratorFieldNameStack.push(m_iteratorFieldName);
				}

				// IF
				if ("if".equalsIgnoreCase(qName)) {
					if (m_execute) {
						String testAttribute = m_attributes.get("test");
						String patternAttribute = m_attributes.get("pattern");
						
						Document testValue = executeEnricher(testAttribute, m_document, m_document, testAttribute, context);
						m_executeElse = (patternAttribute!=null) ? !testValue.equals(patternAttribute) : testValue.isEmpty();
					}
				} else
		
				// THEN
				if ("then".equalsIgnoreCase(qName)) {
					m_execute = m_execute && !m_executeElse;
				} else
		
				// ELSE
				if ("else".equalsIgnoreCase(qName)) {
					m_execute = m_execute && m_executeElse;
				} else		
				
				// SWITCH
				if ("switch".equalsIgnoreCase(qName)) {
					if (m_execute) {
						String testAttribute = m_attributes.get("test");
						m_testValue = executeEnricher(testAttribute, m_document, m_document, testAttribute, context);
						m_executeElse = true;
						m_executeElseStack.pop();
						m_executeElseStack.push(m_executeElse);						
					}
				} else
					
				// CASE
				if ("case".equalsIgnoreCase(qName)) {
					if (m_execute) {
						String patternAttribute = m_attributes.get("pattern");
						m_execute = (patternAttribute != null) ? m_testValue.equals(patternAttribute) : !m_testValue.isEmpty();
						if (m_execute) {
							m_executeElse = false;
							m_executeElseStack.pop();
							m_executeElseStack.push(m_executeElse);						
						}
					}
				} else

				// SELECT
				if ("select".equalsIgnoreCase(qName)) {
					if (m_execute) {
						String testAttribute = m_attributes.get("test");
						m_testValue = executeEnricher(testAttribute, m_document, m_document, testAttribute, context);

						String iteratorFieldNameAttribute = m_attributes.get("iterator");
						if (iteratorFieldNameAttribute != null) {
							m_iteratorFieldName = iteratorFieldNameAttribute;
						} else {
							if ((m_iteratorFieldName != null) && "_iterator_".equals(m_iteratorFieldName)) {
								throw new SAXException("Default iterator name '_iterator_' is already in use; use 'iterator' attribute to specify a unique iterator name.");
							}
							m_iteratorFieldName = "_iterator_";
						}
					}
				} else

				// DO
				if ("do".equalsIgnoreCase(qName)) {
					if (m_execute) {
						String patternAttribute = m_attributes.get("pattern");
		
						// CREATE SUBDOCUMENT FROM PATTERN
						Document iteratorFieldValueDocument = m_testValue.getDocument(patternAttribute);

						// ADD ITERATOR VALUE TO DOCUMENT
						Collection<Object> iteratorFieldValues = iteratorFieldValueDocument.getFieldValues("0");
						if (iteratorFieldValues != null) {
							m_execute = true;
							m_document.addField(m_iteratorFieldName, iteratorFieldValues);								
						}
					}
				}	
			} catch(Exception e) {
				throw new SAXException("Problem in EnricherSAXHandler::startElement(" + uri + ", " + qName + ", " + qName + ", " + m_attributes + ") at line " + m_locator.getLineNumber() + " and column " + m_locator.getColumnNumber(), e);
			}
		}
	
		/**
		 * Converts a given {@link Attributes} to a map of string to string.
		 * 
		 * @param attributes the {@link Attributes} to convert, cannot be <code>null</code>.
		 * @return the mapping, never <code>null</code>.
		 */
		private Map<String,String> fromAttributes(Attributes attributes) {
			HashMap<String,String> retval = new HashMap<String,String>();
			
			for (int i=0; i<attributes.getLength(); i++) {
				retval.put(/*key*/attributes.getQName(i), /*value*/attributes.getValue(i));
			}
			return retval;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Document handle(Document document, String expression, EnricherContext context) throws EnricherException {
		Document retval = null;
		
		try {
		    EnricherSAXHandler handler = new EnricherSAXHandler();
			retval = handler.handle(document, expression, context);
		} catch(Exception e) {
			throw new EnricherException(e);
		}
		return retval;
	}
}