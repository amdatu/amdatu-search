/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.impl;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLFilterImpl;

/**
 * Provides a SAX filter helper for {@link PipeAndFilterEnricherImpl}.
 */
public class PipeAndFilterEnricherSAXFilter extends XMLFilterImpl {
	private static final String ROOT = "enricher";
	
	private String m_name = null;
	private Boolean m_execute = false;
	
    /**
     * {@inheritDoc}
     */
	@Override
	public void characters(char ch[], int start, int length) {
		if (m_execute) {
			try {
				super.characters(ch, start, length);
			} catch (SAXException e) {
				//ignore
			}
		}
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (m_execute) {
			super.endElement(uri, localName, qName);
		}
		if ((m_name != null) && (ROOT.equalsIgnoreCase(localName))) {
			m_execute = false;
		}
	}

	/**
	 * Returns the name of the tag this filter searches.
	 * 
	 * @return a name, can be <code>null</code>.
	 */
	public String getName() {
		return m_name;
	}
	
	/**
	 * Sets the name of the tag to filter on.
	 * 
	 * @param name the tag name to search for, can be <code>null</code>.
	 */
	public void setName(String name) {
		m_name = name;
	}
	
	/**
	 * {@inheritDoc}
	 */
    @Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if ((m_name != null) && (ROOT.equalsIgnoreCase(localName))) {
			if (attributes.getValue("name").equals(m_name)) {
				m_execute = true;
			}
		}
		if (m_execute) {
			super.startElement(uri, localName, qName, attributes);			
		}
	}
}