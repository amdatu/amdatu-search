/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.impl;

import java.util.Collection;

import org.amdatu.search.Document;
import org.amdatu.search.enricher.EnricherContext;

/**
 * Provides an enricher that add the values of a field matching the given expression.
 */
public class FieldEnricherImpl extends AbstractEnricherImpl {
    /**
     * {@inheritDoc}
     */
    public Document handle(Document document, String expression, EnricherContext context) {
        Document retval = document.newDocument();
        Collection<Object> values = document.getFieldValues(expression);
        if (values != null) {
            retval.addField(values);
        }
        return retval;
    }
}
