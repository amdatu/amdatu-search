/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.impl;

import java.net.URLEncoder;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.amdatu.search.Document;
import org.amdatu.search.enricher.Enricher;
import org.amdatu.search.enricher.EnricherContext;
import org.amdatu.search.enricher.EnricherException;
import org.apache.commons.lang.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * Provides an abstract base implementation of an {@link Enricher}.
 */
public abstract class AbstractEnricherImpl implements Enricher {
    protected static final Pattern PATTERN = Pattern.compile("\\$\\{([\\w\\.]+)\\}");
    
	protected volatile BundleContext m_bundleContext;

	/**
	 * @param name
	 * @param document
	 * @param source
	 * @param expression
	 * @param attributes
	 * @return
	 * @throws EnricherException
	 */
	public Document executeEnricher(String name, Document document, Document source, String expression, EnricherContext attributes) throws EnricherException {
		Document retval = null;
		ServiceReference serviceReference = null;

		try {
		    serviceReference = getServiceReference(name);
			Enricher enricher = (Enricher) m_bundleContext.getService(serviceReference);

			if (enricher != null) {
			    // XXX Marrs: WTF?
				String replacedExpression = enricher.replace(expression, document, null /* encoding */).replaceAll("^[a-zA-Z\\.]*::", ""); 
				retval = enricher.handle(source, replacedExpression, attributes);
			}
		} catch (Exception e) {
			throw new EnricherException(e);
		} finally {
		    if (serviceReference != null) {
		        ungetServiceReference(serviceReference);
		    }
		}
		return retval;
	}
	
    /**
     * {@inheritDoc}
     */
	public Document handle(Document document, String expression) throws EnricherException {
		return handle(document, expression, new EnricherContextImpl());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public abstract Document handle(Document document, String expression, EnricherContext attributes) throws EnricherException;
	
	public String replace(String expression, Document document) {
		return replace(expression, document, null /* encodingScheme */);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String replace(String expression, Document document, String encodingScheme) {
		String retval = new String(expression);
		try {
			String variableName = null;
			
			Matcher matcher = PATTERN.matcher(expression);
			while (matcher.find()) {
				variableName = matcher.group(1);
				Collection<Object> fieldValues = document.getFieldValues(variableName);
				if (fieldValues != null) {
					String fieldValue = ((fieldValues.toArray())[0]).toString();
					
					if (!StringUtils.isEmpty(encodingScheme)) {
						fieldValue = URLEncoder.encode(fieldValue, encodingScheme);
					}
					retval = retval.replaceFirst("\\$\\{" + variableName + "\\}", fieldValue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return retval;
	}

	/**
	 * @param name
	 * @return
	 * @throws EnricherException
	 */
	protected ServiceReference getServiceReference(String name) throws EnricherException {
		ServiceReference retval = null;
		
		try {
			ServiceReference[]serviceReferences = m_bundleContext.getServiceReferences(Enricher.class.getName(), 
					"(&(" +Constants.OBJECTCLASS + "=" + Enricher.class.getName() + ")(" + org.amdatu.search.Constants.ENRICHER_TYPE +"=" + name.replaceAll("::.*","") + "))");
			if ((serviceReferences != null) && (serviceReferences.length > 0)) {
				retval = serviceReferences[0];
			} else {
				return getServiceReference("constant");
			}
		} catch (InvalidSyntaxException ise) {
			throw new EnricherException("Cannot retrieve enricher service of type '" + name + "'", ise);			
		}
		return(retval);
	}

	/**
	 * @param serviceReference
	 */
	protected void ungetServiceReference(ServiceReference serviceReference) {
		m_bundleContext.ungetService(serviceReference);
	}

}
