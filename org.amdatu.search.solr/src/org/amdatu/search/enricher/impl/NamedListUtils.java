/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.impl;

import org.apache.solr.common.util.NamedList;

/**
 * Provides some convenience methods for handling named lists.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class NamedListUtils {
    /**
     * @param name
     * @param values
     * @return
     */
    public static NamedList createNamedList(String name, String[] values) {
        NamedList retval = new NamedList<String[]>();

        String[] parts = name.split("\\/");

        retval.add(parts[parts.length - 1], values);

        for (int i = (parts.length - 2); i >= 0; i--) {
            NamedList part = new NamedList();
            part.add(parts[i], retval);
            retval = part;
        }
        return retval;
    }

    /**
     * @param namedList
     * @param namedListToMerge
     * @return
     */
    public static NamedList mergeNamedLists(NamedList namedList, NamedList namedListToMerge) {
        NamedList retval = (namedList != null) ? namedList.clone() : new NamedList();

        for (int i = 0; ((namedListToMerge != null) && (i < namedListToMerge.size())); i++) {
            String keyToMerge = namedListToMerge.getName(i);
            Object valueToMerge = namedListToMerge.get(keyToMerge);

            Object valueToMergeWith = retval.get(keyToMerge);
            if (((valueToMergeWith != null) && (valueToMergeWith instanceof NamedList)) && (valueToMerge instanceof NamedList)) {
                retval.remove(keyToMerge);
                retval.add(keyToMerge, mergeNamedLists((NamedList) valueToMergeWith, (NamedList) valueToMerge));
            }
            else {
                retval.add(keyToMerge, valueToMerge);
            }
        }
        return retval;
    }
}
