/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.search.enricher.impl;

import java.util.HashMap;
import java.util.Map;

import org.amdatu.search.enricher.EnricherContext;

/**
 * Provides an implementation of {@link EnricherContext} backed by a hash map.
 */
public class EnricherContextImpl implements EnricherContext {

    private final Map<String, Object> m_map;

    /**
     * Creates a new {@link EnricherContextImpl} instance with an empty map.
     */
    public EnricherContextImpl() {
        m_map = new HashMap<String, Object>();
    }

    /**
     * Creates a new {@link EnricherContextImpl} instance with an empty map.
     * 
     * @param map the values of this context, cannot be <code>null</code>.
     */
    public EnricherContextImpl(Map<String, String> map) {
        m_map = new HashMap<String, Object>(map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValue(String key) {
        if (key == null || "".equals(key.trim())) {
            throw new IllegalArgumentException("Key cannot be null or empty!");
        }
        return m_map.get(key);
    }
}
