/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher.impl;

import java.io.IOException;
import java.util.Collection;

import org.amdatu.search.Document;
import org.amdatu.search.enricher.Enricher;
import org.amdatu.search.enricher.EnricherContext;
import org.amdatu.search.enricher.EnricherException;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

/**
 * Provides a base class for {@link SearchComponent}s used for the enricher framework.
 */
public abstract class AbstractEnricherSearchComponent extends SearchComponent {
    private final BundleContext m_bundleContext;
    
    /**
     * Creates a new {@link AbstractEnricherSearchComponent} instance.
     */
    public AbstractEnricherSearchComponent() {
        m_bundleContext = FrameworkUtil.getBundle(getClass()).getBundleContext();
    }

    /**
     * {@inheritDoc}
     */
	public String getDescription() {
		return "Enricher SearchComponent";
	}
	
    /**
     * {@inheritDoc}
     */
	public String getSource() {
		return null;
	}

    /**
     * {@inheritDoc}
     */
	public String getSourceId() {
		return null;
	}

    /**
     * {@inheritDoc}
     */
	public String getVersion() {
		return null;
	}

    /**
     * {@inheritDoc}
     */
	public void prepare(ResponseBuilder rb) throws IOException { 
	    // Nop
	}
	
    /**
     * {@inheritDoc}
     */
	public abstract void process(ResponseBuilder rb) throws IOException;
	
	/**
	 * @param name
	 * @param document
	 * @param source
	 * @param expression
	 * @param context
	 * @return
	 * @throws EnricherException
	 */
	protected final Document executeEnricher(String name, Document document, Document source, String expression, EnricherContext context) throws EnricherException {
		Document retval = null;
		
		try {
			Enricher enricher = getEnricher(name);
			if (enricher != null) {
				String replacedExpression = enricher.replace(expression, document, null /* encoding */).replaceAll("^[a-zA-Z\\.]*::", "");
				retval = enricher.handle(source, replacedExpression, context);
			}
		} catch(Exception e) {
			throw new EnricherException(e);
		}
		return retval;
	}
	
	/**
	 * @param sourceAttribute
	 * @param typeAttribute
	 * @param nameAttribute
	 * @param expression
	 * @param document
	 * @return
	 * @throws EnricherException
	 */
	protected final Document executeEnricher(String sourceAttribute, String typeAttribute, String nameAttribute, String expression, Document document) throws EnricherException {
		// HANDLE SOURCE
		Document sourceValue = document;
		if (sourceAttribute != null) {
			sourceValue = executeEnricher(sourceAttribute, document, document, sourceAttribute, null);
		}
		
		// HANDLE TYPE
		if (typeAttribute != null) {
			if (expression.matches("^[a-zA-Z\\-]*::.*")) {
				Document expressionDocument = executeEnricher(expression, document, document, expression, null);
				if (!expressionDocument.isEmpty()) {
					String name = (String)(expressionDocument.getFieldNames().toArray())[0]; // FIXME use List?
					expression = ((expressionDocument.getFieldValues(name).toArray())[0]).toString(); // FIXME use List?
				}
			}
		} else {
			typeAttribute = "pipe-and-filter";
		}

		// HANDLE ENRICHER
		if ((typeAttribute==null) || !(typeAttribute.startsWith("pipe-and-filter"))) {
			Document value = executeEnricher(typeAttribute, document, sourceValue, expression, null);
			Collection<Object> fieldValues = null;
			if (value != null) {
				if (nameAttribute != null) {
					// GET UNNAMED VALUE
					fieldValues = value.getFieldValues("0");
					if ((fieldValues == null) || (fieldValues.size() <= 0)) {
						fieldValues = value.getFieldValues(nameAttribute);
					} 
					document.addField(nameAttribute, fieldValues);
				} else {
					for (String fieldName : value.getFieldNames()) {		
						fieldValues = value.getFieldValues(fieldName);
						document.addField(fieldName, fieldValues);
					}
				}
			}
		} else {
			document = executeEnricher("pipe-and-filter", document, document, expression, null);
		}
		
		return document;
	}
	
	/**
	 * @param name
	 * @return
	 * @throws EnricherException
	 */
	protected Enricher getEnricher(String name) throws EnricherException {
		Enricher retval = null;
		try {
			ServiceReference[] serviceReferences = m_bundleContext.getServiceReferences(Enricher.class.getName(), 
					"(&(" + Constants.OBJECTCLASS + "=" + Enricher.class.getName() + ")(" + org.amdatu.search.Constants.ENRICHER_TYPE + "=" + name + "))");
			if (serviceReferences != null && serviceReferences.length > 0) {
				retval = (Enricher) m_bundleContext.getService(serviceReferences[0]);
			} 
		} catch(Exception e) {
			throw new EnricherException(e);
		}
		
		return retval;
	}
}
