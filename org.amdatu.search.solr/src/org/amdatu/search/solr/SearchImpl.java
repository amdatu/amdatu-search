/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr;

import static org.amdatu.search.Constants.REPOSITORY_NAME;
import static org.amdatu.search.solr.SolrConfigHelper.createConfigurationFile;
import static org.amdatu.search.solr.SolrConfigHelper.createConfigurationFiles;
import static org.amdatu.search.solr.SolrConfigHelper.createSolrXmlConfig;
import static org.amdatu.search.solr.SolrConfigHelper.verifyConfigurationValues;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.amdatu.search.Document;
import org.amdatu.search.Index;
import org.amdatu.search.Search;
import org.amdatu.search.SearchException;
import org.amdatu.search.SearchResponse;
import org.amdatu.search.document.DocumentImpl;
import org.amdatu.search.document.DocumentWithAttrImpl;
import org.amdatu.search.enricher.Enricher;
import org.amdatu.search.enricher.EnricherContext;
import org.amdatu.search.enricher.impl.EnricherContextImpl;
import org.amdatu.search.solr.jaxb.Metadata;
import org.amdatu.search.solr.jaxb.MetadataList;
import org.amdatu.search.solr.jaxb.SearchResponseImpl;
import org.amdatu.search.solr.jaxb.SolrDocumentImpl;
import org.amdatu.search.solr.jaxb.SolrDocumentImplList;
import org.apache.commons.io.FileUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.CoreContainer;
import org.osgi.service.cm.ConfigurationException;
import org.xml.sax.InputSource;

/**
 * Provides a search engine implementation based on Solr.
 */
@SuppressWarnings({ "rawtypes" })
public class SearchImpl implements Search, Index {
	
	private final String m_storageAreaDir;

	// Injected by DependencyManager...
	private volatile Enricher m_enricher;
	// Set in #updateSolrProperties
	private volatile String m_repositoryName;
	// Set in #initializeRepository
	private volatile EmbeddedSolrServer m_server;
	private volatile CoreContainer m_coreContainer;

	/**
	 * Creates a new {@link SearchImpl} instance.
	 * 
	 * @param storageAreaDir the location where to (relatively) store data, cannot be <code>null</code>.
	 * @throws IllegalArgumentException in case the given directory was <code>null</code> or empty.
	 */
	public SearchImpl(String storageAreaDir) {
		if (storageAreaDir == null || "".equals(storageAreaDir.trim())) {
			throw new IllegalArgumentException("Invalid storage area directory: cannot be null or empty!");
		}
		m_storageAreaDir = storageAreaDir;
	}
	
	/**
	 * Called when the configuration is removed; allows our administration to be cleaned...
	 */
	public void removeRepository() throws IOException {
		if (m_repositoryName != null) {
			FileUtils.deleteDirectory(new File(m_storageAreaDir, m_repositoryName));
			m_repositoryName = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Document> delete(String query) throws SearchException {
		assertCorrectState();
		
		Collection<Document> retval = null;
		try {
			SearchResponse response = select(query, 0, Integer.MAX_VALUE, null);
			retval = response.getResults();
			m_server.deleteByQuery(query);
		    m_server.commit();
		}
		catch (Exception e) {
			throw new SearchException(e);
		}
		return retval;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Document deleteById(String id) throws SearchException {
		assertCorrectState();
		
		Document retval = null;
		try {
			String query = "identifier:" + id;
			SearchResponse response = select(query, 0, 1, null);
			if(response.getResults().size()>0) {
				retval = (Document) (response.getResults().toArray())[0];
				m_server.deleteByQuery(query);
			    m_server.commit();
			}
		}
		catch (Exception e) {
			throw new SearchException(e);
		}
		return retval;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Document newDocument() {
		assertCorrectState();
		return new DocumentImpl();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SearchResponse select(String query, int start, int rows, String sort) throws SearchException {
		return select(query, start, rows, sort, null /* params */);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SearchResponse select(String query, int start, int rows, String sort, Map<String, List<String>> params) throws SearchException {
		assertCorrectState();
		
		SolrDocumentImplList documents = new SolrDocumentImplList();
		MetadataList metadata = null;
		try {		
			SolrQuery request = new SolrQuery().setQuery(query);
			request.setStart(start);
			request.setRows(rows);
			//request.setSortField(field, order)
			
            if (params != null) {
                for (Map.Entry<String, List<String>> entry : params.entrySet()) {
                    String key = entry.getKey();
                    if (!key.equalsIgnoreCase("query") && !key.equalsIgnoreCase("start") && !key.equalsIgnoreCase("rows") && !key.equalsIgnoreCase("sort")) {
                        for (String value : entry.getValue()) {
                            request.add(entry.getKey(), value);
                        }
                    }
                }
            }

			QueryResponse response = m_server.query(request);
			
			SolrDocumentList hits = response.getResults();
            for (SolrDocument document : hits) {
				documents.add(new SolrDocumentImpl(document));
			}
			
			metadata = new MetadataList();
			
			// HEADER 
			Metadata hdr = new Metadata();
			Document dd = new DocumentImpl();
			
			dd.addField("maxScore", hits.getMaxScore());
			dd.addField("numFound", hits.getNumFound());
			dd.addField("start", hits.getStart());
			
			NamedList nl = response.getResponseHeader();
            for (int i = 0; i < nl.size(); i++) {
	        	dd.addField(nl.getName(i), nl.get(nl.getName(i)));
	        }
	        hdr.put("_", dd);
			metadata.put("header", hdr);				


			// HITHIGHLIGHTING
			Map<String, Map<String, List<String>>> highlighting = response.getHighlighting();
			if (highlighting != null) {
				Metadata hl = new Metadata();
				
				for (String key : highlighting.keySet()) {
					Document d = new DocumentImpl();
					Map<String, List<String>> values = highlighting.get(key);
                    for (String k : values.keySet()) {
						d.addField(k, values.get(k));					
					}
					hl.put(key, d);
				}
				metadata.put("highlighting", hl);				
			}
			
			// FACETTED SEARCH
			Metadata fc = new Metadata();

			List<FacetField> facetFields = response.getFacetFields();
			if (facetFields != null) {
				for (FacetField field : facetFields) {
					Document d = new DocumentWithAttrImpl();
					for (final Count count : field.getValues()) {
						d.addField("field", count.getCount(), getFieldAttributes(count.getName()));
					}
					fc.put("field::" + field.getName(), d);
				}
			}		
			
			List<FacetField> facetDates = response.getFacetDates();
			if (facetDates != null) {
				for (FacetField field : facetDates) {
					Document d = new DocumentWithAttrImpl();
					field.getName();
					for (final Count count : field.getValues()) {
						d.addField("date", count.getCount(), getFieldAttributes(count.getName()));
					}
					fc.put("field::" + field.getName(), d);
				}
			}		

			Map<String, Integer> facetQueries = response.getFacetQuery();
			if (facetQueries != null) {
				Document d = new DocumentWithAttrImpl();
				for (final Map.Entry<String, Integer> entry : facetQueries.entrySet()) {
					d.addField("query", entry.getValue(), getFieldAttributes(entry.getKey()));
				}
				fc.put("_", d);
			}
	
			if (!fc.isEmpty()) {
				metadata.put("facet_counts", fc);
			}
			
			// RESPONSE ENRICHER
			NamedList responseNamedList = response.getResponse();
	        for (int i = 0; ((responseNamedList != null) && (i < responseNamedList.size())); i++) {
	        	String responseNamedListKey = responseNamedList.getName(i);
	        	if (!responseNamedListKey.equalsIgnoreCase("responseHeader") && 
        			!responseNamedListKey.equalsIgnoreCase("response") && 
        			!responseNamedListKey.equalsIgnoreCase("facet_counts") && 
        			!responseNamedListKey.equalsIgnoreCase("highlighting")) {
	        		Metadata mtdt = new Metadata();
					Object responseNamedListValue = responseNamedList.getVal(i);
					if ((responseNamedListValue != null) && (responseNamedListValue instanceof NamedList)) {
				        for (int j = 0; (j < ((NamedList) responseNamedListValue).size()); j++) {
							Document d = new DocumentImpl();
							String key = ((NamedList)responseNamedListValue).getName(j);
							Object values = ((NamedList)responseNamedListValue).getVal(j);
                            if (values instanceof NamedList) {
								// AMDATU-METADATAKEY-URI-DOCUMENT
                                for (int k = 0; (k < ((NamedList) values).size()); k++) {
						        	String key2 = ((NamedList)values).getName(k);
						        	d.addField(key2, ((NamedList)values).getAll(key2));
						        }
								mtdt.put(key, d);
							} 
				        }
					}
					metadata.put(responseNamedListKey, mtdt);
	        	}
	        }
		}
		catch (Exception e) {
			throw new SearchException(e);
		}
		return new SearchResponseImpl(documents, metadata);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Collection<Document> documents) throws SearchException {
	    Document[] document = new Document[documents.size()];
	    update(documents.toArray(document));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Document... documents) throws SearchException {
	    assertCorrectState();
	    
	    if (documents == null || documents.length < 1) {
	        throw new IllegalArgumentException("At need one document is needed to update!");
	    }

        try {
            EnricherContext context = new EnricherContextImpl();
            
            String expr = createExpression("index");

            Collection<SolrInputDocument> addDocuments = new ArrayList<SolrInputDocument>();
            for (Document document : documents) {
                Document enrichedDocument = m_enricher.handle(document, expr, context);
                SolrInputDocument addDocument = new SolrInputDocument();
                for (String name : enrichedDocument.getFieldNames()) {
                    Collection<Object> values = document.getFieldValues(name);
                    for (Object value : values) {
                        addDocument.addField(name, value);                      
                    }
                }
                addDocuments.add(addDocument);
            }
            m_server.add(addDocuments);
            m_server.commit();
        }
        catch (Exception e) {
            throw new SearchException(e);
        }
	}

	/**
	 * Updates the runtime configuration of this service.
	 * 
	 * @param properties the properties to get the configuration from, cannot be <code>null</code>.
	 * @throws ConfigurationException in case the given configuration contains errors.
	 */
	public void updateSolrProperties(Dictionary properties) throws ConfigurationException {
		Object rnValue = properties.get(REPOSITORY_NAME);
		if (rnValue == null || !(rnValue instanceof String)) {
			throw new ConfigurationException(REPOSITORY_NAME, "Missing or invalid value!");
		}

		if ((m_repositoryName != null) && !m_repositoryName.equals(rnValue)) {
			throw new ConfigurationException(REPOSITORY_NAME, "Cannot change name of repository!");
		}
		
		// Keep the name of the repository for later use...
		m_repositoryName = (String) rnValue;

		// Initialize the (new) repository...
		initializeRepository(m_repositoryName, properties);
	}
	
	/**
	 * Sets the enricher for updating the documents.
	 * 
	 * @param enricher the enricher to set, should not be <code>null</code>.
	 */
	final void setEnricher(Enricher enricher) {
		m_enricher = enricher;
	}
	
	/**
	 * Asserts that the public API methods aren't called while this object is in an incorrect state.
	 * 
	 * @throws IllegalStateException in case this object is in an incorrect state.
	 */
	private void assertCorrectState() throws IllegalStateException {
		if (m_repositoryName == null || m_server == null) {
			throw new IllegalStateException("Search not correctly initialized!");
		}
	}
	
	/**
	 * Creates a valid expression for use in an {@link Enricher}.
	 * 
	 * @param expr the part of the expression to pass to the enricher, cannot be <code>null</code>.
	 * @return an enricher expression, never <code>null</code>.
	 * @throws IllegalStateException in case this method was called without having a repository to work on.
	 */
	private String createExpression(String expr) throws IllegalStateException {
		if (m_repositoryName == null) {
			throw new IllegalStateException("No repository name given!");
		}
		String part = String.format("conf/%s#%s", SolrConfigHelper.ENRICHER_XML, expr);
		return new File(new File(m_storageAreaDir, m_repositoryName), part).getAbsolutePath();
	}

	/**
	 * Creates a map representing the field attributes.
	 * 
	 * @param value the value to return in the attributes, cannot be <code>null</code>.
	 * @return a map with the entry { "value", given parameter }.
	 */
	private Map<String, String> getFieldAttributes(final String value) {
		return Collections.singletonMap("value", value);
	}

	/**
	 * Initializes the repository by updating or creating it.
	 * 
	 * @param repositoryName the name of the repository to update/create;
	 * @param properties the configuration for the repository.
	 * @throws ConfigurationException in case an invalid configuration was supplied.
	 */
	private void initializeRepository(String repositoryName, Dictionary properties) throws ConfigurationException {
		// INITIALIZE SOLR's HOME DIRECTORY
		File repoHomeDir = new File(m_storageAreaDir, repositoryName);
		repoHomeDir.mkdirs();
		String repoHome = repoHomeDir.getAbsolutePath();

		// INITIALIZE CONF DIRECTORY
		File confHomeDir = new File(repoHome, "conf");
		confHomeDir.mkdirs();
		String confHome = confHomeDir.getAbsolutePath();

		// INITIALIZE RESOURCES
		verifyConfigurationValues(properties);

		createConfigurationFiles(properties, confHome);

		// INITIALIZE SERVER
		ClassLoader original = Thread.currentThread().getContextClassLoader();

		try {
			Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

			if (m_coreContainer != null) {
				m_coreContainer.reload(repositoryName);
            }
            else {
				m_coreContainer = new CoreContainer();

				// INITIALIZE SOLR.XML
				InputSource solrConfig = createSolrXmlConfig(repoHome, repositoryName);

				// INITIALIZE SOLRCORE.PROPERTIES
				Properties solrcore_properties = new Properties();
				solrcore_properties.put("request.enricher.expression", createExpression("search.request"));
				solrcore_properties.put("response.enricher.expression", createExpression("search.response"));
				createConfigurationFile(solrcore_properties, confHome + "/solrcore.properties");

				m_coreContainer.load(repoHome, solrConfig);
			}

			if (m_server == null) {
				m_server = new EmbeddedSolrServer(m_coreContainer, repositoryName);
			}
        }
        catch (Exception e) {
            throw new ConfigurationException(REPOSITORY_NAME, "Cannot initialize server!", e);
        }
        finally {
			Thread.currentThread().setContextClassLoader(original);
		}
	}
}
