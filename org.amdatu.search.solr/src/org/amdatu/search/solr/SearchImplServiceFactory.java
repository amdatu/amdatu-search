/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashMap;

import org.amdatu.search.Index;
import org.amdatu.search.Search;
import org.amdatu.search.enricher.Enricher;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Provides the {@link ManagedServiceFactory} implementation for creating new {@link SearchImpl} instances on demand.
 */
public class SearchImplServiceFactory implements ManagedServiceFactory {
	/** Factory PID of this service factory. */
	public static final String FACTORY_PID = "org.amdatu.search.solr";
	
	private static final String NAME = FACTORY_PID.concat(" Factory");

	private volatile BundleContext m_context;
	private volatile DependencyManager m_manager;
	private volatile LogService m_log;

	private final HashMap<String, Component> m_instances = new HashMap<String, Component>();

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void updated(String pid, Dictionary properties) throws ConfigurationException {
        m_log.log(LogService.LOG_DEBUG, "Configuration received for service PID: " + pid);
	    SearchImpl search = null;
	    Component component = null;
	    boolean addComponent = false;
	    synchronized (m_instances) {
            component = m_instances.get(pid);
	        if (component == null) {
	            m_log.log(LogService.LOG_DEBUG, "Creating search repository for service PID: " + pid);
	            String dataDir = m_context.getDataFile("").getAbsolutePath();
                search = new SearchImpl(dataDir);
	            
	            component = m_manager.createComponent()
                    .setInterface(new String[] {Search.class.getName(), Index.class.getName()}, properties)
                    .setImplementation(search)
                    .add(m_manager.createServiceDependency()
                        .setService(Enricher.class, "(&(" + Constants.OBJECTCLASS + "=" + Enricher.class.getName() + ")(" + org.amdatu.search.Constants.ENRICHER_TYPE + "=pipe-and-filter))")
                        .setRequired(true));
	            
	            m_instances.put(pid, component);
	            addComponent = true;
	        }
	    }
	    if (search == null) {
	    	m_log.log(LogService.LOG_DEBUG, "Updating search repository for service PID: " + pid);
	        search = (SearchImpl) component.getService();
	    }
	    if (search != null) {
	        search.updateSolrProperties(properties);
	        if (addComponent) {
	            m_manager.add(component);
	        }
	    }
	    else {
	        m_log.log(LogService.LOG_WARNING, "Configuration update could not be applied, search service not found. Service PID: " + pid);
	    }
	}

	@Override
	public void deleted(String pid) {
        m_log.log(LogService.LOG_DEBUG, "Configuration deleted for service PID: " + pid);
	    Component component;
	    synchronized (m_instances) {
	        component = m_instances.remove(pid);
	    }
		if (component != null) {
			// Service exists; let's take it our of business...
			SearchImpl search = (SearchImpl) component.getService();
			m_manager.remove(component);
			if (search != null) {
				// Service exists; let's clean it up...
				try {
				    m_log.log(LogService.LOG_DEBUG, "Removing search repository for service PID: " + pid);
					search.removeRepository();
				}
				catch (IOException e) {
					m_log.log(LogService.LOG_WARNING, "Failed to clean up search service! PID = " + pid, e);
				}
			}
			else {
				m_log.log(LogService.LOG_WARNING, "Configuration delete could not be applied, search service not found. Ignoring delete of its properties. Service PID: " + pid);
			}
		}
	}
}
