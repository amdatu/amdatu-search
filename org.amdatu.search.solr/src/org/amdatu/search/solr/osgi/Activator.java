/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr.osgi;

import java.util.Properties;

import org.amdatu.search.Index;
import org.amdatu.search.Search;
import org.amdatu.search.enricher.Enricher;
import org.amdatu.search.enricher.impl.ConstantEnricherImpl;
import org.amdatu.search.enricher.impl.FieldEnricherImpl;
import org.amdatu.search.enricher.impl.PipeAndFilterEnricherImpl;
import org.amdatu.search.solr.SearchImplServiceFactory;
import org.amdatu.search.solr.rest.SearchRestImpl;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
	
	@Override
	public void init(final BundleContext context, final DependencyManager manager) throws Exception {
		Properties props = new Properties();
		props.put(Constants.SERVICE_PID, SearchImplServiceFactory.FACTORY_PID);

		manager.add(createComponent()
			.setInterface(ManagedServiceFactory.class.getName(), props)
			.setImplementation(SearchImplServiceFactory.class)
            .add(createServiceDependency()
        		.setService(LogService.class)
        		.setRequired(false))
        );

		manager.add(createComponent()
			.setInterface(Object.class.getName(), null)
			.setImplementation(SearchRestImpl.class)
			.add(createServiceDependency()
			    .setService(Search.class)
			    .setCallbacks("addSearch", "removeSearch")
		    )
			.add(createServiceDependency()
			    .setService(Index.class)
			    .setCallbacks("addIndex", "removeIndex")
		    )
		);
		
	    props = new Properties();
		props.put(org.amdatu.search.Constants.ENRICHER_TYPE, "pipe-and-filter");
		
        manager.add(createComponent()
			.setInterface(Enricher.class.getName(), props)
			.setImplementation(PipeAndFilterEnricherImpl.class)
		);

	    props = new Properties();
		props.put(org.amdatu.search.Constants.ENRICHER_TYPE, "constant");

        manager.add(createComponent()
			.setInterface(Enricher.class.getName(), props)
			.setImplementation(ConstantEnricherImpl.class)
		);

	    props = new Properties();
		props.put(org.amdatu.search.Constants.ENRICHER_TYPE, "field");

        manager.add(createComponent()
			.setInterface(Enricher.class.getName(), props)
			.setImplementation(FieldEnricherImpl.class)
		);
	}
	
	@Override
	public void destroy(final BundleContext context, final DependencyManager manager) throws Exception {
		// No-op
	}
}
