/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr.jaxb;

import java.net.URLDecoder;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class JaxbDocumentAdapter extends XmlAdapter<Object, org.amdatu.search.Document> { 
	@Override
	public org.amdatu.search.Document unmarshal(Object domTree) {
		org.amdatu.search.Document retval = new org.amdatu.search.document.DocumentImpl();
		
		NodeList childNodes = ((Element)domTree).getChildNodes();
		if (childNodes.getLength() > 0) {
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node child = childNodes.item(i);
				String name = child.getNodeName();
				if(!name.startsWith("#")) {
					String value = ((Text) child.getChildNodes().item(0)).getWholeText();
					retval.addField(name, value);
				}
			}
		}
		return(retval);
	}

	@Override
	public Object marshal(org.amdatu.search.Document document) {
		Element retval = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document domDocument = db.newDocument();
			
			retval = marshal(domDocument.createElement("Document"), document);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(retval);
	}
	
	public Element marshal(Element element, org.amdatu.search.Document document) {
		try {
			Document domDocument = element.getOwnerDocument();
			for(Object key : document.getFieldNames()) {			
				if(StringUtils.equalsIgnoreCase((String)key, "identifier")) {
					Collection<Object> values = document.getFieldValues(key.toString());
					for(Object value : values) {
						element.setAttribute("id", URLDecoder.decode(value.toString(), "UTF-8"));
						break;
					}
				} else
				if(StringUtils.equalsIgnoreCase((String)key, "score")) {
					Collection<Object> values = document.getFieldValues(key.toString());
					for(Object value : values) {
						element.setAttribute("score", String.valueOf(value));
						break;
					}
				} else {
					for(Object values : document.getFieldValues(key.toString())) {
						if(values instanceof Collection) {
							for(Object value : (Collection<?>)values) {
								Element e = domDocument.createElement(key.toString());
								e.appendChild(domDocument.createTextNode(value.toString()));
								element.appendChild(e);
							}
						} else 
						if(values instanceof org.amdatu.search.document.Field) {
							Element e = domDocument.createElement(key.toString());
							e.appendChild(domDocument.createTextNode(values.toString()));
							Map<String,String> attributes = ((org.amdatu.search.document.Field)values).getAttributes();
							if(attributes!=null) {
								for(Entry<String, String> attribute : attributes.entrySet()) {
									Attr attributeElement = domDocument.createAttribute(attribute.getKey());
									attributeElement.appendChild(domDocument.createTextNode(attribute.getValue()));
									e.setAttributeNode(attributeElement);
								}
							}
							element.appendChild(e);							
						} else {
							Element e = domDocument.createElement(key.toString());
							e.appendChild(domDocument.createTextNode(values.toString()));
							element.appendChild(e);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(element);
	}

} 