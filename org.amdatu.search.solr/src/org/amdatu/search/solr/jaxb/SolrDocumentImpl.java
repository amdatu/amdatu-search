/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr.jaxb;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.amdatu.search.document.DocumentImpl;
import org.apache.solr.common.SolrDocument;

@XmlRootElement(name="document") 
@XmlAccessorType(XmlAccessType.NONE)
@XmlJavaTypeAdapter(JaxbDocumentAdapter.class) 
public class SolrDocumentImpl extends DocumentImpl {
	protected SolrDocument document;
	
	public SolrDocumentImpl() {
		document = new SolrDocument();
	}
	
	public SolrDocumentImpl(SolrDocument document) {
		this.document = document;
	}

	@Override
	public Collection<String> getFieldNames() {
		return(document.getFieldNames());
	}

	@Override
	public Collection<Object> getFieldValues(String name) {
		return(document.getFieldValues(name));
	}
			
	@Override
	public void addField(String name, Object value) {
		document.addField(name, value);
	}

	@Override
	public Object removeField(String name) {
		Collection<Object> retval = (Collection<Object>)getFieldValues(name);
		if(retval!=null) {
			document.remove(name);
		}
		return(retval);
	}
}
