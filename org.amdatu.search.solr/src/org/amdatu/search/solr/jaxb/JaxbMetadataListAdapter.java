/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr.jaxb;

import java.net.URLDecoder;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class JaxbMetadataListAdapter extends XmlAdapter<Object, MetadataList> { 
	JaxbDocumentAdapter documentAdapter = new JaxbDocumentAdapter();
	
	@Override
	public MetadataList unmarshal(Object domTree) {
		return(null);
	}

	@Override
	public Object marshal(MetadataList metadataList) {
		Element retval = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document domDocument = db.newDocument();
			
			retval = domDocument.createElement("Metadata");
			for(Object key : metadataList.keySet()) {
				Element element = domDocument.createElement(key.toString());
				
				Metadata metadata = metadataList.get(key);
				for(String k : metadata.keySet()) {
					Element document = domDocument.createElement("document");
					if(!k.equals("_")) {
						document.setAttribute("idref", URLDecoder.decode(k.toString(), "UTF-8"));
					}
					
					document = 	documentAdapter.marshal(document, metadata.get(k));
					element.appendChild(document);
				}
				retval.appendChild(element);
			}
			return(retval);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(retval);
	} 
} 