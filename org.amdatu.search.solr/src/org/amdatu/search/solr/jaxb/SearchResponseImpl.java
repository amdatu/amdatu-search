/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr.jaxb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.amdatu.search.Document;
import org.amdatu.search.SearchResponse;

@XmlRootElement(name="SearchResponse") 
@XmlAccessorType(XmlAccessType.NONE)
public class SearchResponseImpl implements SearchResponse {	
    @XmlElementWrapper
	@XmlElement(name="document")
	private SolrDocumentImplList results;
	
	@XmlJavaTypeAdapter(JaxbMetadataListAdapter.class) 
	private  MetadataList metadata;
		
	public SearchResponseImpl() {
	}

	public SearchResponseImpl(SolrDocumentImplList results) {
		this.results = results;
	}
	
	public SearchResponseImpl(SolrDocumentImplList results,	MetadataList metadata) {
		super();
		this.results = results;
		this.metadata = metadata;
	}

    public SolrDocumentImplList getDocumentList() {
		return(results);
    }
    
    public Collection<Document> getResults() {
    	Collection<Document> retval = new ArrayList<Document>();
    	for(Document document : results) {
    		retval.add(document);
    	}
		return(retval);
	}
	
	public Map<String, Document> getMetadata(String name) {
		return(metadata.get(name));
	}
}
