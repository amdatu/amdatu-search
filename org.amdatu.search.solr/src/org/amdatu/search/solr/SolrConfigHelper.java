/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr;

import static org.amdatu.search.Constants.ENRICHER_CONFIG;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_ENGINE;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_SCHEMA;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_STOPWORDS;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_SYNONYMS;
import static org.amdatu.search.Constants.REPOSITORY_CONFIG_URLWORDS;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Dictionary;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.osgi.service.cm.ConfigurationException;
import org.xml.sax.InputSource;

/**
 * Provides some utility methods for creating the configuration (files) of Solr.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
final class SolrConfigHelper {
	static final String ENRICHER_XML = "/enricher.xml";
	static final String SCHEMA_XML = "/schema.xml";
	static final String SOLRCONFIG_XML = "/solrconfig.xml";
	static final String STOPWORDS_TXT = "/stopwords.txt";
	static final String SYNONYMS_TXT = "/synonyms.txt";
	static final String URLWORDS_TXT = "/urlwords.txt";

	/**
	 * Creates a "static" {@link InputSource} containing the "solr.xml" 
	 * configuration for a repository with a given name and home.
	 * 
	 * @param repoHome the location of the repository;
	 * @param repositoryName the name of the repository.
	 * @return an {@link InputSource} instance with the "solr.xml" data.
	 */
	static InputSource createSolrXmlConfig(String repoHome, String repositoryName) {
		String solr_xml = String.format(
			"<solr persistent=\"true\" sharedLib=\"lib\">" +
		 	"<cores adminPath=\"/admin/cores\" defaultCoreName=\"%1$s\"><core name=\"%1$s\" instanceDir=\"%2$s\"/></cores>" +
			"</solr>", repositoryName, repoHome);
		InputSource solrConfig = new InputSource(new ByteArrayInputStream(solr_xml.getBytes()));
		return solrConfig;
	}

	/**
	 * Creates a configuration file with the contents of the property denoted by the given dictionary and key.
	 * 
	 * @param properties the dictionary to take the file contents from;
	 * @param key the key under which the file contents are stored in the given dictionary;
	 * @param outputFile the file name to write the contents to.
	 * @throws ConfigurationException in case we failed writing the configuration file.
	 */
	static void createConfigurationFile(Dictionary properties, String key, String outputFile) throws ConfigurationException {
		try {
			FileUtils.writeStringToFile(new File(outputFile), (String) properties.get(key));
		}
		catch (IOException e) {
			throw new ConfigurationException(key, "Failed to create file!", e);
		}
	}
	
	/**
	 * Creates a configuration file with the contents of the given properties file.
	 * 
	 * @param properties the properties file to write to file;
	 * @param outputFile the file name to write the properties to.
	 * @throws ConfigurationException in case we failed writing the configuration file.
	 */
	static void createConfigurationFile(Properties properties, String outputFile) throws ConfigurationException {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(outputFile);
			properties.store(fos, "");
		}
		catch (IOException e) {
			throw new ConfigurationException(outputFile, "Failed to create file!", e);
		}
		finally {
			IOUtils.closeQuietly(fos);
		}
	}

	/**
	 * Creates a configuration file with the values of the given dictionary.
	 * 
	 * @param properties the properties file to write to file;
	 * @param outputFile the file name to write the properties to.
	 * @throws ConfigurationException in case we failed writing the configuration file.
	 */
	static void createConfigurationFiles(Dictionary properties, String baseDir) throws ConfigurationException {
		createConfigurationFile(properties, REPOSITORY_CONFIG_SCHEMA, baseDir + SCHEMA_XML);
		createConfigurationFile(properties, REPOSITORY_CONFIG_ENGINE, baseDir + SOLRCONFIG_XML);
		createConfigurationFile(properties, REPOSITORY_CONFIG_STOPWORDS, baseDir + STOPWORDS_TXT);
		createConfigurationFile(properties, REPOSITORY_CONFIG_SYNONYMS, baseDir + SYNONYMS_TXT);
		createConfigurationFile(properties, REPOSITORY_CONFIG_URLWORDS, baseDir + URLWORDS_TXT);
		createConfigurationFile(properties, ENRICHER_CONFIG, baseDir + ENRICHER_XML);
	}
	
	/**
	 * Verifies whether the given properties contain the correct value for the given key, filling it with a default if missing.
	 * 
	 * @param properties the dictionary to verify, cannot be <code>null</code>;
	 * @param key the name of the property to verify, cannot be <code>null</code>;
	 * @param defaultResource the name of the default resource to use, cannot be <code>null</code>.
	 * @throws ConfigurationException in case the given default resource could not be read properly.
	 */
	static void verifyConfigurationValue(Dictionary properties, String key, String defaultResource) throws ConfigurationException {
		Object value = properties.get(key);
		if (value == null || !(value instanceof String)) {
			InputStream is = null;
			try {
				is = SolrConfigHelper.class.getResourceAsStream(defaultResource);
				if (is == null) {
					throw new ConfigurationException(key, "Failed to find default value: " + defaultResource + "!");
				}

				properties.put(key, IOUtils.toString(is));
			}
			catch (Exception e) {
				throw new ConfigurationException(key, "Failed to read default value!", e);
			}
			finally {
				IOUtils.closeQuietly(is);
			}
		}
	}
	
	/**
	 * Verifies whether the given properties contain all correct values, filling them with defaults if necessary.
	 * 
	 * @param properties the dictionary to verify, cannot be <code>null</code>.
	 * @throws IOException in case of I/O problems reading one of the resources.
	 */
	static void verifyConfigurationValues(Dictionary properties) throws ConfigurationException {
		verifyConfigurationValue(properties, REPOSITORY_CONFIG_SCHEMA, SCHEMA_XML);
		verifyConfigurationValue(properties, REPOSITORY_CONFIG_ENGINE, SOLRCONFIG_XML);
		verifyConfigurationValue(properties, REPOSITORY_CONFIG_STOPWORDS, STOPWORDS_TXT);
		verifyConfigurationValue(properties, REPOSITORY_CONFIG_SYNONYMS, SYNONYMS_TXT);
		verifyConfigurationValue(properties, REPOSITORY_CONFIG_URLWORDS, URLWORDS_TXT);
		verifyConfigurationValue(properties, ENRICHER_CONFIG, ENRICHER_XML);
	}
}
