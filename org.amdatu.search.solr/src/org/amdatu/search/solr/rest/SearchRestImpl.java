/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.solr.rest;

import static org.amdatu.search.Constants.REPOSITORY_NAME;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.amdatu.search.Document;
import org.amdatu.search.Index;
import org.amdatu.search.Search;
import org.amdatu.search.SearchException;
import org.amdatu.search.solr.jaxb.SolrDocumentImplList;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

@Path("search")
public class SearchRestImpl {
    private final Map<String, Search> m_searchServices = new ConcurrentHashMap<String, Search>();
    private final Map<String, Index> m_indexServices = new ConcurrentHashMap<String, Index>();

    @GET
	@Path("{repository}/select")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response select(
    			@Context final UriInfo info, 
	    		@PathParam("repository") String repository,
	    		@DefaultValue("*:*") @QueryParam("query") String query,
	            @DefaultValue("0") @QueryParam("start") int start,
	            @DefaultValue("10") @QueryParam("rows") int rows,
	            @DefaultValue("score desc") @QueryParam("sort") String sort) 
    		throws SearchException, InvalidSyntaxException {

    	Search search = m_searchServices.get(repository);
		if (search != null) {
			return Response.ok().entity(search.select(query, start, rows, sort, info.getQueryParameters())).build();
		}
		else {
		    return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("No search service found for repository '" + repository + "'.").build();
		}
    }
    
    @POST
	@Path("{repository}/index")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response updateDocuments(
	    		@Context final HttpHeaders headers, 
	    		@Context final HttpServletRequest request,
	    		@PathParam("repository") String repository,
	    		SolrDocumentImplList documents) 
    		throws SearchException, InvalidSyntaxException {

    	SolrDocumentImplList retval = documents;
		Index index = m_indexServices.get(repository);
    	if (index != null) {
    		for (Document document : documents) {
    			index.update(document);
    		}
            return Response.ok().entity(retval).build();
    	}
    	else {
            return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("No index service found for repository '" + repository + "'.").build();
    	}
    }	
    
    @PUT
	@Path("{repository}/index/{id}")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response updateDocument(
	    		@Context final HttpHeaders headers, 
	    		@Context final HttpServletRequest request,
	    		@PathParam("repository") String repository,
	    		@PathParam("id") String id,
	    		SolrDocumentImplList documents) 
    		throws SearchException, InvalidSyntaxException {

    	SolrDocumentImplList retval = new SolrDocumentImplList();

		Index index = m_indexServices.get(repository);
    	if (index != null) {
    		for (Document document : documents) {
    			Collection<Object> values = document.getFieldValues("uri");
    			if ((values != null) && (values.equals(id))) {
    				index.update(document);
    				retval.add(document);
    			}
    		}    		
            return Response.ok().entity(retval).build();
    	}
        else {
            return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("No index service found for repository '" + repository + "'.").build();
        }
    }	

    @DELETE
	@Path("{repository}/index")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response delete(
	    		@Context final HttpHeaders headers, 
	    		@Context final HttpServletRequest request,
	    		@PathParam("repository") String repository,
	    		@DefaultValue("*:*") @QueryParam("query") String query) 
    		throws SearchException, InvalidSyntaxException {

		Index index = m_indexServices.get(repository);
    	if (index != null) {
    		Collection<Document> deletedDocuments = index.delete(query);
    		if (deletedDocuments != null) {
    		    return Response.ok().entity(new SolrDocumentImplList(deletedDocuments)).build();
    		}
    		else {
                return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("No documents in repository '" + repository + "' matched the delete query '" + query + "'.").build();
    		}
        }
        else {
            return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("No index service found for repository '" + repository + "'.").build();
        }
    }	
    
    @DELETE
	@Path("{repository}/index/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response deleteById(
	    		@Context final HttpHeaders headers, 
	    		@Context final HttpServletRequest request,
	    		@PathParam("repository") String repository,
	    		@PathParam("id") String id) 
			throws SearchException, InvalidSyntaxException {

		Index index = m_indexServices.get(repository);
    	if (index != null) {
    		Document deletedDocument = index.deleteById(id);
    		if (deletedDocument != null) {
    		    return Response.ok().entity(new SolrDocumentImplList(deletedDocument)).build();
    		}
            else {
                return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("No document in repository '" + repository + "' matched the provided identity '" + id + "'.").build();
            }
        }
        else {
            return Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("No index service found for repository '" + repository + "'.").build();
        }
    }

    public void addSearch(ServiceReference ref, Search search) {
        String name = getRepositoryName(ref);
        if (name != null) {
            m_searchServices.put(name, search);
        }
    }
    
    public void removeSearch(ServiceReference ref, Search search) {
        String name = getRepositoryName(ref);
        if (name != null) {
            m_searchServices.remove(name);
        }
    }
    
    public void addIndex(ServiceReference ref, Index index) {
        String name = getRepositoryName(ref);
        if (name != null) {
            m_indexServices.put(name, index);
        }
    }

    public void removeIndex(ServiceReference ref, Index index) {
        String name = getRepositoryName(ref);
        if (name != null) {
            m_indexServices.remove(name);
        }
    }
    
    private String getRepositoryName(ServiceReference ref) {
        return (String) ref.getProperty(REPOSITORY_NAME);
    }
}
