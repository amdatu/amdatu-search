/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.client;

import org.amdatu.search.Document;
import org.amdatu.search.Index;
import org.amdatu.search.Search;
import org.amdatu.search.SearchException;
import org.amdatu.search.SearchResponse;

public class SearchClient {
	private volatile Index m_index;
	private volatile Search m_search;
	
	public void start() {
		try {
			Document doc = m_index.newDocument();
			doc.addField("uri", "http://www.manning.com/hatcher3");
			doc.addField("title", "Lucene in Action, 2nd Edition");
			doc.addField("description", 
					"When Lucene first appeared, this superfast search engine was nothing short of amazing. " +
					"Today, Lucene still delivers. Its high-performance, easy-to-use API, features like numeric " +
					"fields, payloads, near-real-time search, and huge increases in indexing and searching speed " +
					"make it the leading search tool. And with clear writing, reusable examples, and unmatched " +
					"advice, Lucene in Action, Second Edition is still the definitive guide to effectively " +
					"integrating search into your applications. This totally revised book shows you how to index " +
					"your documents, including formats such as MS Word, PDF, HTML, and XML. It introduces you to " +
					"searching, sorting, and filtering, and covers the numerous improvements to Lucene since the " +
					"first edition. Source code is for Lucene 3.0.1.");
			m_index.update(doc);
	
			Document doc1 = m_index.newDocument();
	    	doc1.addField("uri", "http://www.manning.com/teutelink");
	    	doc1.addField("title", "Enterprise Search in Action");
	    	doc1.addField("description", 
	    			"Enterprise Search in Action is an in depth guide for developers and architects that explores " +
	    			"enterprise search from many unique angles. It begins with the architecture of a typical enterprise " +
	    			"search solution and then dives deep into the nitty-gritty details of real world enterprise search use " +
	    			"cases. As you read, you'll pick up valuable technical details about Lucene and Solr, Tika, Mule, Nutch, " +
	    			"OpenRDF, and other core technologies. More importantly, you'll see how each plays its part in creating a " +
	    			"complete enterprise search solution.");

			m_index.update(doc1);
			
			long t1, t2;
			t1 = System.currentTimeMillis();
			Document[] docs = new Document[1000];
			for (int i = 0; i < docs.length; i++) {
				docs[i] = m_index.newDocument();
				docs[i].addField("uri", "http://www.manning.com/marrs-" + i);
				docs[i].addField("title", "Amdatu Search " + i);
				docs[i].addField("description", randomize(
		    			"Enterprise Search in Action is an in depth guide for developers and architects that explores " +
		    			"enterprise search from many unique angles. It begins with the architecture of a typical enterprise " +
		    			"search solution and then dives deep into the nitty-gritty details of real world enterprise search use " +
		    			"cases. As you read, you'll pick up valuable technical details about Lucene and Solr, Tika, Mule, Nutch, " +
		    			"OpenRDF, and other core technologies. More importantly, you'll see how each plays its part in creating a " +
		    			"complete enterprise search solution. Plus some random shit: " + i));
			}
			m_index.update(docs);
			t2 = System.currentTimeMillis();
			
			System.out.println("Insert: " + (t2 - t1) + " ms.");

			t1 = System.currentTimeMillis();
			SearchResponse response = m_search.select("random", 0, 10, null);
			for(Document document : response.getResults()) {
				for(String name : document.getFieldNames()) {
					for(Object value : document.getFieldValues(name)) {
						System.out.println(name + "=" + value);									
					}
				}
			}
			t2 = System.currentTimeMillis();
			System.out.println("Search: " + (t2 - t1) + " ms.");
		}
		catch (SearchException e) {
			e.printStackTrace();
		}
	}

	private String randomize(String string) {
		return string.replace("aeouimstnr".charAt((int) (Math.random() * 10)), "mstnraeoui".charAt((int) (Math.random() * 10)));
	}
}
