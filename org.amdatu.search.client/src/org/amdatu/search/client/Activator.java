/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.client;

import static org.amdatu.search.Constants.REPOSITORY_NAME;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Properties;

import org.amdatu.search.Index;
import org.amdatu.search.Search;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

public class Activator extends DependencyActivatorBase {
	/** Factory PID of the service factory. */
	public static final String FACTORY_PID = "org.amdatu.search.solr";
	
	private volatile ConfigurationAdmin m_configAdmin;

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		manager.add(createComponent()
			.setImplementation(SearchClient.class)
			.add(createServiceDependency()
				.setService(Search.class, "(&(" + Constants.OBJECTCLASS + "=" + Search.class.getName() + ")(" + REPOSITORY_NAME + "=amdatu))")
				.setRequired(true))
			.add(createServiceDependency()
				.setService(Index.class, "(&(" + Constants.OBJECTCLASS + "=" + Index.class.getName() + ")("+ REPOSITORY_NAME + "=amdatu))")
				.setRequired(true)));
		
		manager.add(createComponent()
			.setImplementation(this)
			.add(createServiceDependency()
				.setService(ConfigurationAdmin.class)
				.setRequired(true)));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void start() throws IOException {
		Configuration config = m_configAdmin.createFactoryConfiguration(FACTORY_PID, null);
		Dictionary props = new Properties();
		props.put(REPOSITORY_NAME, "amdatu");
		
		config.update(props);
	}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception {
	}
}
