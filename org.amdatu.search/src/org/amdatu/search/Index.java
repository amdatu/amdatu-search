/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search;

import java.util.Collection;

/**
 * Provides access to the search repository allowing mutations on the repository to be performed.
 */
public interface Index {
    /**
     * Deletes documents from a search repository based upon the given query.
     * 
     * @param query the query to match the documents against, cannot be <code>null</code>.
     * @return A collection with all deleted documents
     * @throws SearchException in case the deletion failed for one of the documents.
     */
    public Collection<Document> delete(String query) throws SearchException;

    /**
     * Deletes the document that is identified by the given parameter.
     * 
     * @param id the identification of the document to be deleted from the index, cannot be <code>null</code>.
     * @return the deleted {@link Document}, or <code>null</code> if no document matched the given identification.
     * @throws SearchException in case the deletion failed.
     */
    public Document deleteById(String id) throws SearchException;

    /**
     * Factory method for creating a new document instance.
     * 
     * @return a new document instance, never <code>null</code>.
     */
    public Document newDocument();

    /**
     * Adds a new, or updates an existing document, within a search repository.
     * 
     * @param documents the collection of documents to add/update, cannot be <code>null</code>.
     * @deprecated use {@link #update(Document...)} instead.
     * @throws SearchException in case the update failed for one of the documents.
     */
    public void update(Collection<Document> documents) throws SearchException;

    /**
     * Adds a new, or updates an existing document, within a search repository.
     * 
     * @param documents the array with documents to add or update, cannot be <code>null</code>.
     * @throws IllegalArgumentException in case the given documents was <code>null</code> or an empty array;
     * @throws SearchException in case the update failed for one of the documents.
     */
    public void update(Document... documents) throws SearchException;
}
