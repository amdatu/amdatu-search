/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.search.enricher;

/**
 * Provides context information about the current environment to an {@link Enricher}.
 */
public interface EnricherContext {
    /**
     * Returns a context value for a given key.
     * 
     * @param key the name of the context value to retrieve, cannot be <code>null</code> or empty.
     * @return a context value, can be <code>null</code>.
     * @throws IllegalArgumentException in case the given key was <code>null</code> or empty.
     */
    Object getValue(String key);
}
