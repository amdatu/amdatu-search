/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher;

import java.util.Map;

import org.amdatu.search.Document;

/**
 * Denotes a enricher for search documents, allowing them to be "enriched" with additional meta data.
 */
public interface Enricher {

    /**
     * Handles a given document and enriches it using the given expression and attributes.
     * 
     * <p>Each enricher takes, besides an input document, an expression that explains it what to do. For example, if the
     * enricher is implementing a XPath-extraction kind of enrichment, the expression could be an XPath expression.
     * Similarly, if the 'filter-and-pipe' enricher uses the expression to delegate the actual enrichment to another
     * enricher implementation.</p>
     * 
     * @param document the document on which the enricher should do its work.
     * @param expression the expression that is to be executed by the enricher, cannot be <code>null</code>;
     * @param context the context that can be used by enrichers to get context information, cannot be <code>null</code>.
     * @return the enriched document, never <code>null</code>.
     * @throws EnricherException in case of problems during the enrichment.
     */
    public Document handle(Document document, String expression, EnricherContext context) throws EnricherException;

    /**
     * @see #handle(Document, String)
     * @deprecated use {@link #handle(Document, String, Map)} with a <code>null</code> value for the attributes
     *             parameter instead.
     */
    public Document handle(Document document, String expression) throws EnricherException;

    /**
     * Replaces all occurrences of expressions in the form of '${key}' with the field values of the given document.
     * 
     * @param expression the expression to replace all occurrences in, cannot be <code>null</code>;
     * @param document the document to take all field values from, cannot be <code>null</code>;
     * @param encodingScheme the (optional) URL encoding scheme to apply, when <code>null</code> no encoding will take
     *        place, assuming the result will remain UTF8.
     * @return the given expression with all matched ocurrences replaced, never <code>null</code>.
     */
    public String replace(String expression, Document document, String encodingScheme);

    /**
     * @see #replace(String, Document)
     * @deprecated use {@link #replace(String, Document, String)} with a <code>null</code> value for the encodingScheme
     *             parameter instead.
     */
    public String replace(String expression, Document document);
}
