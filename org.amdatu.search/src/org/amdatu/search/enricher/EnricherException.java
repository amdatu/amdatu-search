/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search.enricher;

/**
 * Denotes an exception that can be thrown from {@link Enricher}s to denote something went wrong during the enrichment
 * process.
 */
@SuppressWarnings("serial")
public class EnricherException extends Exception {
    private final Object m_payload;

    /**
     * Creates a new {@link EnricherException} instance.
     */
    public EnricherException() {
        m_payload = null;
    }

    /**
     * Creates a new {@link EnricherException} instance with a given payload.
     * 
     * @param payload the payload to wrap in this exception, can be used for additional information passing.
     */
    public EnricherException(Object payload) {
        this.m_payload = payload;
    }

    /**
     * Creates a new {@link EnricherException} instance.
     * 
     * @param message the message of this exception.
     */
    public EnricherException(String message) {
        super(message);
        m_payload = null;
    }

    /**
     * Creates a new {@link EnricherException} instance.
     * 
     * @param message the message of this exception;
     * @param payload the payload to wrap in this exception, can be used for additional information passing.
     */
    public EnricherException(String message, Object payload) {
        super(message);
        this.m_payload = payload;
    }

    /**
     * Creates a new {@link EnricherException} instance.
     * 
     * @param message the message of this exception;
     * @param cause the originating cause of this exception.
     */
    public EnricherException(String message, Throwable cause) {
        super(message, cause);
        m_payload = null;
    }

    /**
     * Creates a new {@link EnricherException} instance.
     * 
     * @param message the message of this exception;
     * @param cause the originating cause of this exception;
     * @param payload the payload to wrap in this exception, can be used for additional information passing.
     */
    public EnricherException(String message, Throwable cause, Object payload) {
        super(message, cause);
        m_payload = payload;
    }

    /**
     * Creates a new {@link EnricherException} instance.
     * 
     * @param cause the originating cause of this exception.
     */
    public EnricherException(Throwable cause) {
        super(cause);
        m_payload = null;
    }

    /**
     * Returns the payload of this exception.
     * 
     * @return the (optional) payload of this exception, can be <code>null</code>.
     */
    public Object getPayload() {
        return m_payload;
    }
}
