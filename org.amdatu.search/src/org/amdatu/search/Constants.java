/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search;

import org.amdatu.search.enricher.Enricher;

/**
 * Provides some common used constants for this bundle.
 */
public interface Constants {

    /**
     * The name of the 'unnamed' field used in {@link Document}s and {@link Enricher}s.
     */
    String UNNAMED_FIELD = "0";

    /**
     * Denotes the type of an enricher, used to uniquely identify an enricher. 
     */
    String ENRICHER_TYPE = "enricher-type";

    /**
     * This property specifies the name of the search repository that is used by the search interface.
     */
    String REPOSITORY_NAME = "repository";

    /**
     * This property specifies the XML-based configuration string of the schema file that defines the fields and types
     * of the search repository�s schema. The schema is expected to adhere Lucene/SOLR schema format. <p> If this
     * property is not specified the schema will default back to the extended Dublin core. </p>
     * 
     * @see http://wiki.apache.org/solr/SchemaXml
     * @see http://en.wikipedia.org/wiki/Dublin_Core
     */
    String REPOSITORY_CONFIG_SCHEMA = "schema";

    /**
     * This property specifies the XML-based configuration string of the runtime behavior of the search repository. The
     * config is expected to adhere the Lucene/SOLR configuration format. <p> If this property is not specified the
     * configuration will default back to a default search and update handler configuration without any specific
     * parameters. </p>
     * 
     * @see http://wiki.apache.org/solr/SolrConfigXml
     */
    String REPOSITORY_CONFIG_ENGINE = "config";

    /**
     * This property specifies a list of stop words that are filtered from the search documents. <p> The given list
     * should have a word per line. </p>
     */
    String REPOSITORY_CONFIG_STOPWORDS = "stopwords";

    /**
     * This property specifies a list of synonyms. <p> The given list should have synonyms per line, where each word is
     * separated by a comma. </p>
     */
    String REPOSITORY_CONFIG_SYNONYMS = "synonyms";

    /**
     * This property specifies a list of URL words. <p> The given list should have a word per line. </p>
     */
    String REPOSITORY_CONFIG_URLWORDS = "urlWords";

    /**
     * This property specifies the XML-based configuration string of the request and response enrichment behavior.
     */
    String ENRICHER_CONFIG = "enricher";

}
