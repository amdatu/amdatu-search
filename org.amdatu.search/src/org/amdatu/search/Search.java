/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search;

import java.util.List;
import java.util.Map;

/**
 * The search instance will provide read access to the required search repository via the select method specified
 */
public interface Search {

    /**
     * Searches through a search repository
     * 
     * @param query the search query, when <code>null</code> defaults to <code>*:*</code> returning
     *        everything;
     * @param start the starting position, used for pagination of the response, >= 0;
     * @param rows the amount of documents to return, used for pagination, >= 0;
     * @param sort the field + order to sort on, when <code>null</code> defaults to score in ascending order.
     * @return the search response, containing the results of the query, never <code>null</code>.
     * @throws SearchException in case the search failed somehow.
     */
    public SearchResponse select(String query, int start, int rows, String sort) throws SearchException;

    /**
     * Searches through a search repository
     * 
     * @param query the search query, when <code>null</code> defaults to <code>*:*</code> returning
     *        everything;
     * @param start the starting position, used for pagination of the response, >= 0;
     * @param rows the amount of documents to return, used for pagination, >= 0;
     * @param sort the field + order to sort on, when <code>null</code> defaults to score in ascending order;
     * @param params the (optional) parameters of the query, may be <code>null</code>.
     * @return the search response, containing the results of the query, never <code>null</code>.
     * @throws SearchException in case the search failed somehow.
     */
    public SearchResponse select(String query, int start, int rows, String sort, Map<String, List<String>> params) throws SearchException;
}
