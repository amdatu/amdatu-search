/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search;

import java.util.Collection;
import java.util.Map;

/**
 * Denotes a response of a search query.
 */
public interface SearchResponse {
    /**
     * Returns the matching results of the search query.
     * 
     * @return a collection of {@link Document}s, never <code>null</code>.
     */
    public Collection<Document> getResults();

    /**
     * Returns the metadata of a field with the given name.
     * 
     * @param name the name of the field to return the metadata for, cannot be <code>null</code>.
     * @return a map with metadata, can be <code>null</code> in case the given name did not yield a valid field in the
     *         result.
     */
    public Map<String, Document> getMetadata(String name);
}