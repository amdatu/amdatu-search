/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.search;

import java.util.Collection;
import java.util.Map;

/**
 * Represents an entity in the search backend, which consists of metadata in the form of fields and sub-documents.
 */
public interface Document {
    /**
     * Returns the unique identity of this document.
     */
    public String getId();
    /**
     * This method can be used to create a new Document implementation instance.
     * 
     * @return A {@link Document} implementation instance, never <code>null</code>.
     */
    public Document newDocument();

    /**
     * This method can be used to retrieve a collection with the names of all fields within the document. This might
     * include the unnamed fields, which internal name is "0".
     * 
     * @return A collection with field names, never <code>null</code>.
     */
    public Collection<String> getFieldNames();

    /**
     * This method can be used to retrieve a collection with all values from a specific field within the document. To
     * retrieve the values of an unnamed field, use the name "0".
     * 
     * @return A collection with field values, never <code>null</code>.
     */
    public Collection<Object> getFieldValues(String name);

    /**
     * This method can be used to add values to an unnamed field in the document. The field will be named "0" under the
     * hood and can be named explicitly within the pipe-and-filter enricher using the name attribute. The value can be
     * either a single value or a collection of values, in which case all individual values within the collection will
     * be added.
     * 
     * @param value the initial value of the unnamed field to add.
     */
    public void addField(Object value);

    /**
     * This method can be used to add values and attributes of that value to an unnamed field in the document. The field
     * will be named "0" under the hood and can be named explicitly within the pipe-and-filter enricher using the name
     * attribute. The value can be either a single value or a collection of values, in which case all individual values
     * within the collection will be added.
     * 
     * @param value the initial value of the unnamed field to add;
     * @param attributes the attributes of the unnamed field to add.
     */
    public void addField(Object value, Map<String, String> attributes);

    /**
     * This method can be used to add values to a specific field in the document. The value can be either a single value
     * or a collection of values, in which case all individual values within the collection will be added.
     * 
     * @param name the name of the field to add, cannot be <code>null</code> or empty;
     * @param value the initial value of the field to add.
     */
    public void addField(String name, Object value);

    /**
     * This method can be used to add values and attributes of that value to a specific field in the document. The value
     * can be either a single value or a collection of values, in which case all individual values within the collection
     * will be added.
     * 
     * @param name the name of the field to add, cannot be <code>null</code> or empty;
     * @param value the initial value of the field to add;
     * @param attributes the (optional) attributes of the field to add.
     */
    public void addField(String name, Object value, Map<String, String> attributes);

    /**
     * This method can be used to replace the value from a specific field in the document with a new value. The value
     * can be either a single value or a collection of values, in which case all individual values within the collection
     * will be replaced.
     * 
     * @param name the name of the field to set, cannot be <code>null</code> or empty;
     * @param value the new value of the field to set.
     */
    public void setField(String name, Object value);

    /**
     * This method can be used to replace the value and attributes of the value from a specific field in the document
     * with a new value and attributes. The value can be either a single value or a collection of values, in which case
     * all individual values within the collection will be replaced.
     * 
     * @param name the name of the field to set, cannot be <code>null</code> or empty;
     * @param value the new value of the field to set;
     * @param attributes the (optional) attributes of the field to set.
     */
    public void setField(String name, Object value, Map<String, String> attributes);

    /**
     * This method can be used to remove a specific field from the document. The removed field values will be returned.
     * 
     * @param name the name of the field to remove, cannot be <code>null</code> or empty.
     * @return the removed field values, can be <code>null</code> in case the given field does not exist.
     */
    public Object removeField(String name);

    /**
     * This method can be used to retrieve a map with all fields within the document.
     * 
     * @return a map with all fields, never <code>null</code>.
     */
    public Map<String, Collection<Object>> getFields();

    /**
     * This method can be used to extract a sub document from the document, containing all values that comply with the
     * supplied regular expression based pattern.
     * 
     * @param pattern the regular expression to apply, cannot be <code>null</code> or empty.
     * @return a sub document, can be <code>null</code> in case no sub document matched the given regular expression.
     */
    public Document getDocument(String pattern);
    
    /**
     * Returns whether this document has no field values defined.
     * 
     * @return <code>true</code> if no field values exist, <code>false</code> otherwise.
     */
    public boolean isEmpty();
}
